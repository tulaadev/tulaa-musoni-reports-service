from django.urls import path
from .views import (
    branch_portfolio_report,
    arrears_collection_report,
    portfolio_performance_trend,
    collections_data,
    loan_officer_arrears
)

urlpatterns = [
    path('branch-portfolio-report', branch_portfolio_report),
    path('arrears-repayments-reports', arrears_collection_report),
    path('portfolio-trend', portfolio_performance_trend),
    path('officer-par', collections_data),
    path('loan-officer', loan_officer_arrears)
]
