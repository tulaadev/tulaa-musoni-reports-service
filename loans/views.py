import re
import datetime
import requests
from django.http import HttpResponse, JsonResponse
from urllib.parse import quote
from utils.branch_par import branch_total_par_summary
from utils.helpers import convert_txt_to_matrix, convert_json_to_csv, convert_matrix_to_json, validate_date
from utils.arrears_collection import arrears_collection_amount
from utils.portfolio_trend import portfolio_trend_day
from utils.loans_collections import collections
from utils.loan_arrears import get_loan_arrears_agent
import csv
from rest_framework.decorators import api_view
from musoni import settings


URL = str('https://demo.musonisystem.com:8443/api/v1')
USER = 'tulaa-test-client'
PWD = 'demo123'
ENV_URL = 'https%3A%2F%2Fdemo.musonisystem.com%2F'


def branch_portfolio_report(request):
    """
    To filter by a particular date add a the following query param to your get request:
            - [url]/?date='yyyy-mm-dd'
    The date marks the end date of interest
    :param request:
    :return: json object with a summary of all loans at risk grouped by range
    """

    try:
        date = request.GET['date']
        date = validate_date(date)
    except:
        date = str(datetime.date.today())

    if isinstance(date, JsonResponse):
        return date

    rst = requests.get(
        settings.MUSONI_URL + quote('/runreports/Branch Portfolio Report', safe='/'),
        auth=(settings.MUSONI_USER, settings.MUSONI_PWD),
        params={
            'R_selectOffice': 1,
            'R_toDate': date,
            'R_environementUrl': settings.MUSONI_ENV_URL,
            'R_productGroupId': -1,
            'R_productId': -1,
            'R_currencyDigit': 2,
            'locale': 'en',
            'output-type': 'CSV',
        },
        headers={
            'X-Fineract-Platform-TenantId': 'tulaa'
        }
    )
    # Some data manipulations here. To make sure the the data coming from the server is consistent
    # for processing
    loans_arr = convert_txt_to_matrix(rst)
    rst = branch_total_par_summary(loans_arr, 9, -4)

    return JsonResponse(rst, safe=False)


def arrears_collection_report(request):
    arrears_field = 'total due(pif)'
    repayment_field = 'actual amount repaid'
    try:
        startDate = request.GET['startDate']
        startDate = validate_date(startDate)
    except:
        startDate = str(datetime.date.today())

    try:
        endDate = request.GET['endDate']
        endDate = validate_date(endDate)
    except:
        endDate = str(datetime.date.today())

    if isinstance(startDate, JsonResponse):
        return startDate

    if isinstance(endDate, JsonResponse):
        return endDate

    rst_repayments = requests.get(
        settings.MUSONI_URL + quote('/runreports/Expected_Repayment'),
        auth=(settings.MUSONI_USER, settings.MUSONI_PWD),
        params={
            'R_selectOffice': 1,
            'R_endDate': endDate,
            'R_startDate': startDate,
            'R_selectLoanofficer': -1,
            'R_environementUrl': settings.MUSONI_ENV_URL,
            'R_productGroupId': -1,
            'R_productId': 1,
            'R_currencyDigit': 2,
            'locale': 'en',
            'output-type': 'CSV',
        },
        headers={
            'X-Fineract-Platform-TenantId': 'tulaa'
        }
    )
    repayments = convert_txt_to_matrix(rst_repayments)
    repayments = arrears_collection_amount(repayments, 0, repayment_field)

    rst_arrears = requests.get(
        settings.MUSONI_URL + quote('/runreports/Institution Arrears Report'),
        auth=(settings.MUSONI_USER, settings.MUSONI_PWD),
        params={
            'R_selectOffice': 1,
            'R_toDate': endDate,
            'R_environementUrl': settings.MUSONI_ENV_URL,
            'R_currencyDigit': 2,
            'locale': 'en',
            'output-type': 'CSV',
        },
        headers={
            'X-Fineract-Platform-TenantId': 'tulaa'
        }
    )
    arrears = convert_txt_to_matrix(rst_arrears)
    arrears = arrears_collection_amount(arrears, 1, arrears_field)

    res = [{
        "OverdueLoanAmount": arrears,
        "RealizedCollections": repayments
    }]
    convert_json_to_csv(res, 'arrears-repayment')
    return JsonResponse(res, safe=False)


def portfolio_performance_trend(request):
    cashflow_type = 'Cashflow per day'
    header = 'Date :'
    try:
        date = request.GET['date']
        date = validate_date(date)
    except:
        date = str(datetime.date.today())

    try:
        fields = request.GET['fields'].split(',')
    except:
        fields = []

    if isinstance(date, JsonResponse):
        return date

    rst_repayments = requests.get(
        settings.MUSONI_URL + quote('/runreports/Expected Repayments'),
        auth=(settings.MUSONI_USER, settings.MUSONI_PWD),
        params={
            'R_selectOffice': 1,
            'R_endDate': date,
            'R_startDate': '2019-01-01',
            'R_environementUrl': settings.MUSONI_ENV_URL,
            'R_productGroupId': -1,
            'R_productId': 1,
            'R_currencyDigit': 2,
            'locale': 'en',
            'output-type': 'CSV',
        },
        headers={
            'X-Fineract-Platform-TenantId': 'tulaa'
        }
    )
    repayments = convert_txt_to_matrix(rst_repayments)

    repayments = portfolio_trend_day(repayments, cashflow_type, header, 0)

    # Rename the headers for the repeating columns in Expected columns
    for i in range(len(repayments[0][6:])):
        repayments[0][i + 6] = 'Expected {}'.format(repayments[0][i + 6])

    # filter the response to include only the requested fields
    for i in range(len(repayments[0])):
        repayments[0][i] = repayments[0][i].replace(' ', '')

    column_to_add = []
    if len(fields) > 0:
        for f in fields:
            try:
                repayments[0].index(f)
                column_to_add.append(repayments[0].index(f))
            except:
                continue

        # Perform the filtering
        parsed_rst = []
        for row in repayments:
            sel_col = [row[col] for col in column_to_add]
            parsed_rst.append(sel_col)

        for i in range(len(parsed_rst[1:])):
            for j in range(len(parsed_rst[1:][i])):
                if j != 0:
                    parsed_rst[1:][i][j] = float(parsed_rst[1:][i][j].replace(',', ''))

        repayments = parsed_rst

    convert_matrix_to_json(repayments, 'portfolio-trend-actual', 6)

    # convert_matrix_to_json(repayments, 'portfolio-trend-expected')
    return JsonResponse(repayments, safe=False)


def collections_data(request):
    """
        Returns PAR per agent
        To filter by a particular date add a the following query param to your get request:
                - [url]/?date='yyyy-mm-dd'
        The date marks the end date of interest
        :param request:
        :return: json object with a summary of all loans at risk grouped by range and by loan
        officer
    """

    try:
        date = request.GET['date']
        date = validate_date(date)
    except:
        date = str(datetime.date.today())

    if isinstance(date, JsonResponse):
        return date

    rst = requests.get(
        settings.MUSONI_URL + quote('/runreports/Branch Portfolio Report', safe='/'),
        auth=(settings.MUSONI_USER, settings.MUSONI_PWD),
        params={
            'R_selectOffice': 1,
            'R_toDate': date,
            'R_environementUrl': settings.MUSONI_ENV_URL,
            'R_productGroupId': -1,
            'R_productId': 1,
            'R_currencyDigit': 2,
            'locale': 'en',
            'output-type': 'CSV',
        },
        headers={
            'X-Fineract-Platform-TenantId': 'tulaa'
        }
    )
    # Some data manipulations here. To make sure the the data coming from the server is consistent
    # for processing
    loans_arr = convert_txt_to_matrix(rst)
    rst = collections(loans_arr, 9, -4)

    return JsonResponse(rst, safe=False)


@api_view(['GET'])
def loan_officer_arrears(request):
    """
        Only takes in an agent id
        Returns a loan officers detailed arrears in terms of:
            - PAR%
            - Farmers details
    """

    try:
        agent_id = int(request.GET['agentId'])
    except:
        return HttpResponse('Invalid parameter "agentId". agentId should be a number.agentId should be provided')

    try:
        date = request.GET['date']
        date = validate_date(date)
    except:
        date = str(datetime.date.today())

    if isinstance(date, JsonResponse):
        return date

    rst = requests.get(
        settings.MUSONI_URL + quote('/runreports/Loanofficer Arrears Report', safe='/'),
        auth=(settings.MUSONI_USER, settings.MUSONI_PWD),
        params={
            'R_selectOffice': 1,
            'R_toDate': date,
            'R_environementUrl': settings.MUSONI_ENV_URL,
            'R_selectLoanofficer': agent_id,
            'R_currencyId': 1,
            'output-type': 'CSV',
        },
        headers={
            'X-Fineract-Platform-TenantId': 'tulaa'
        }
    )

    if rst.status_code == 200:
        # Some data manipulations here. To make sure the the data coming from the server is consistent
        # for processing
        arrears = convert_txt_to_matrix(rst)
        rst = get_loan_arrears_agent(arrears)
        return JsonResponse(rst, safe=False)

    return JsonResponse({
        'error': "Not available"
    }, safe=False)
