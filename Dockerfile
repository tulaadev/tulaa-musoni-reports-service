FROM python:3.7
ENV PYTHONBUFFERED 1
RUN mkdir /api
WORKDIR /api

ADD . /api/
RUN pip install -r requirements.txt
 CMD exec gunicorn musoni.wsgi:application --bind 0.0.0.0:8000 --workers 1 --timeout 200 --graceful-timeout 200