## Dev environment setup in linux
1. Install python
    ```bash
    sudo apt install python3.7
    ```
2. Install pip - python package manager
    ```bash
    sudo apt install python3-pip
    ``` 
3. Install python virtual environment
    ```bash
    sudo apt install python3-venv
    ```
4. Install PyCharm or use IntelliJ Ultimate edition

## Run application 
1. Create a virtual environment using python3 module, venv

    ```bash
    python3 -m venv /path/to/new/virtual/environment
    ```
   
2. Activate the virtual environment 

    ```bash
    source /path/to/new/virtual/environment/bin/activate
    ```
   
3. Install the dependencies using pipe

    ```bash
   pip3 install -r requirements.txt
   ```
   
   When installing the requirement you might get error
   ```You need to install postgresql-server-dev-X.Y for building a server-side extension```

   To solve the error install following packages.
   
   ```bash
   sudo apt-get install postgresql
   sudo apt-get install python-psycopg2
   sudo apt-get install libpq-dev
   ```
   then rerun pip3 install -r requirements.txt
4. run the application

    ```bash
    python3 manage.py runserver
    ```
   


## Build a docker image.
```bash
sudo docker-compose build /path/to/dockerfile
```
use this website as a guide on how to deploy to ECS
`https://dzone.com/articles/deploying-docker-containers-to-aws-ecs`


