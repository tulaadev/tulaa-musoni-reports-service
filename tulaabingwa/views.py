"""
Here resides all the endpoints being used in the TulaaBingwa app. It consists of three views:
- arrears_status_view: returns an agents arrears as well as farmers in that category that have arrears that is between
1-30 days
- collections_view: provides collections on a per agent basis
- loan_officer_history_view: shows all loans where that loan officer is no longer the same as the original loan officer

"""
import datetime
import os
import traceback
from time import time

import requests
from django.http import JsonResponse
from rest_framework.decorators import api_view, authentication_classes
from rest_framework.response import Response

from auth_custom.authentication import read_from_pickle
from auth_custom.tulaa_backend import TulaaBackend
from musoni import settings
from musoni.settings import BASE_DIR
from utils.agent_collections import loan_officer_collections_per_farmer
from utils.binary_search import binary_search
from utils.binary_search import binary_search_index
from utils.helpers import load_data, generate_field_mappings, map_agent, validate_date, convert_txt_to_matrix

logger = settings.logger
path = os.path.join(BASE_DIR, 'data/loan_schedule.csv')
loan_schedule = load_data(path)
loan_schedule_totals = len(loan_schedule)
counter = 0

FIELDS = [
    'Loan Officer Id',
    'Loan Officer Name',
    'Client Id',
    'Client Name',
    'Phone Number',
    'Account No',
    'Principal Amount',
    'Arrears Days',
    'Arrears Amount',
    'Principal Repaid Derived',
    'Principal Outstanding Derived',
    'Arrears Date',
    'Total Expected Repayment Derived',
    'Total Repayment Derived',
    'Loan Status Id',
    "First Loan Officer Name",
    "First Loan Officer Id"
]


def client_expected_payment(account_no):
    """
    The method returns the expected payment to date for this loan, from the loan schedule
    :param account_no: unique loan ID
    :return:
    """
    global counter
    today = datetime.datetime.today().date()
    expected_payment = 0
    # Search for this loan in the loan schedule. The schedule has all loans and is big, hence use binary search.
    # The data was sorted earlier
    client_index = int(binary_search_index(loan_schedule[1:], 0, len(loan_schedule) - 2, int(account_no), 10, 0))
    if client_index < 0:  # means this loan was not found
        return 0

    # get the first instance of this client
    while int(loan_schedule[client_index][10]) == account_no:
        client_index -= 1
    client_index += 1

    # start from the first instance and add up all payments for this client
    while client_index < loan_schedule_totals and int(loan_schedule[client_index][10]) == account_no:
        if datetime.datetime.strptime(loan_schedule[client_index][8], "%Y-%m-%d").date() <= today:
            expected_payment += float(loan_schedule[client_index][6])
        client_index += 1
    return expected_payment


# Mapped according to the naming coming from the data export csv

# Create your views here.
@api_view(['GET'])
@authentication_classes([TulaaBackend])
def arrears_status_view(request):
    """
    :param request: take in only one param `agentId: <uuid>`
    :return:
    On success:
            {

               "loan_officer": string,
               "total_par": number,
               "farmers": [
                {
                   "client_name": string,
                   "phone_number": string,
                   "arrears": number
                }
            ],
               "par30": number
            }
    on error:
          {
            'error': string,
            "message": string,
            'error_description': string,
            'status': number
        }
    """
    # tt = time()
    try:
        agent_id = request.GET['agentId']
    except:
        settings.logger.error("An error occurred while getting agent id: " + str(traceback.format_exc()))
        return JsonResponse({
            'error': "Invalid parameter",
            "message": 'Invalid parameter "agentId"',
            'error_description': 'AgentId should be provided in the params',
            'status': 400
        }, status=400, safe=False)

    # Map provided agent id to musoni staff id
    try:
        agent_mapping = map_agent(agent_id)
        if isinstance(agent_mapping, dict):
            agent_id = agent_mapping['data']['identity']['staffId']
        else:
            return JsonResponse({
                'error': "Not found",
                "message": 'Agent not found',
                "error_description": 'Agent with that id does not exist',
                "status": 400
            }, status=404)

        # create a dynamic path to the arrears status data dump
        path = os.path.join(BASE_DIR, 'data/arrears.csv')
        arrears = load_data(path)

        field_map = generate_field_mappings(FIELDS, arrears[0])

        # Filter by an agent id
        # Get all the arrears where this agent was the first loan officer or the current loan officer
        agent = [r for r in arrears[1:] if
                 r[field_map['firstloanofficerid']] == str(agent_id) or r[field_map['loanofficerid']] == str(agent_id)]
        loan_officer_name = ''
        print("agents size", len(agent))
        if len(agent) > 0:
            # Calculate total PAR for entire period and 1-30 Days
            total_arrears_par_30 = 0
            total_arrears = 0
            total_repayment = 0
            total_expected_repayment_ls = 0
            overpayment = 0
            #
            count = 0
            for record in agent:
                count += 1
                # This part is just for getting this loan officer name. And I don't think android app needs this. Liaise with android app to remove this part
                if record[field_map['loanofficerid']] == str(agent_id):
                    loan_officer_name = record[field_map['loanofficername']]
                # some active loans have loan status ID as 'restructured.'
                if record[field_map['loanstatusid']] == 'Active' or record[field_map['loanstatusid']] == 'Restructured':
                    # get arrears that have over 30 days for Par30 calculation
                    if record[field_map['arrearsdays']] and int(record[field_map['arrearsdays']]) > 30:
                        total_arrears_par_30 += float(record[field_map['arrearsamount']])

                    # Get arrears that are from 1 days for par calculation
                    if record[field_map['arrearsdays']] and int(record[field_map['arrearsdays']]) > 0:
                        total_arrears += float(record[field_map['arrearsamount']])
                    # Get the total paid by this farmer so far
                    total_repayment_this_farmer = 0.00 if record[field_map['totalrepaymentderived']] == "" else float(
                        record[field_map['totalrepaymentderived']])
                    # Get the amount this farmer is expected to have paid till today. We are getting this from the loan schedule
                    expected_payment_this_farmer = client_expected_payment(int(record[field_map['accountno']]))
                    # If this farmer has paid more than expected to date, this is overpayment
                    overpayment_this_farmer = total_repayment_this_farmer - expected_payment_this_farmer

                    # sum up to get totals for this agent
                    if overpayment_this_farmer > 0:
                        overpayment += overpayment_this_farmer
                    total_repayment += total_repayment_this_farmer
                    total_expected_repayment_ls += expected_payment_this_farmer

            if total_expected_repayment_ls > 0:
                par30 = round(total_arrears_par_30 / total_expected_repayment_ls * 100, 2)
            else:
                par30 = 0

            if total_expected_repayment_ls > 0:
                # we used to subtract overpayment from the total arrears but Sarah from P&G proved its not a good idea
                par = round(total_arrears / total_expected_repayment_ls * 100, 2)
            else:
                par = 0

            result = {
                'loan_officer': loan_officer_name,
                'total_par': par,
                'total_par_30': par30,
                'total_expected_repayment_ls': total_expected_repayment_ls,
                'total_repayment': total_repayment,
                'overpayment': overpayment,
                'total_arrears': total_arrears,
                'total_arrears_par_30': total_arrears_par_30,
                'current_farmers': [],
                'escalated_farmers': []
            }

            # Push the number of days in arrears per farmer into a dict
            for farmer in agent:
                # Farmers who are currently under this agent and they have arrears
                if farmer[field_map['loanofficerid']] == str(agent_id) and farmer[field_map['arrearsdays']] and int(
                        farmer[field_map['arrearsdays']]) > 0:
                    farmer_arrears = farmer[field_map['arrearsamount']]
                    days_in_arrears = farmer[field_map['arrearsdays']]
                    if farmer_arrears == "":
                        farmer_arrears = '0'
                    if days_in_arrears == "":
                        days_in_arrears = '0'
                    result['current_farmers'].append({
                        'client_name': farmer[field_map['clientname']],
                        'arrears': float(farmer_arrears),
                        'number': farmer[field_map['phonenumber']],
                        'days_in_arrears': int(days_in_arrears)
                    })
                # Farmers who have been moved from this agent and they have arrears
                if farmer[field_map['loanofficerid']] != str(agent_id) and farmer[field_map['arrearsdays']] and int(
                        farmer[field_map['arrearsdays']]) > 0:

                    farmer_arrears = farmer[field_map['arrearsamount']]
                    days_in_arrears = farmer[field_map['arrearsdays']]
                    if farmer_arrears == "":
                        farmer_arrears = '0'
                    if days_in_arrears == "":
                        days_in_arrears = '0'
                    result['escalated_farmers'].append({
                        'client_name': farmer[field_map['clientname']],
                        'arrears': float(farmer_arrears),
                        'number': farmer[field_map['phonenumber']],
                        'days_in_arrears': int(days_in_arrears)
                    })

        else:
            result = {
                'message': "No data available for this agent"
            }
        # logging.info("Time taken" + str(time() - tt))
        # logger.debug("Time taken -> " + str(time() - tt))
        return Response(result)
    except:
        logger.error("An error occurred: " + str(traceback.format_exc()))
        return Response({
            'error': "Internal server error",
            "message": 'Internal server error',
            "error_description": 'Something went wrong. Contact support or try again later',
            "status": 500
        }, status=500)


@api_view(['GET'])
@authentication_classes([TulaaBackend])
def collections_view(request):
    """
        :param request: take in only one param `agentId: <uuid>`
        On success:
                 {
                   "collections": number,
                   "farmers": [
                        {
                           "name": string,
                           "number": string,
                           "repaid": number,
                           "arrears": number,
                           "last_repayment_date": date,
                        }
                    ],
                }

        on error:
              {
                'error': string,
                "message": string,
                'error_description': string,
                'status': number
            }
    """
    tt = time()
    logger.debug("fetching collections report")
    try:
        agent_id = request.GET['agentId']
    except:
        settings.logger.error("An error occurred while getting agent id: " + str(traceback.format_exc()))
        return JsonResponse({

            'error': "Invalid parameter",
            "message": 'Invalid parameter "agentId"',
            'error_description': 'AgentId should be provided in the params',
            'status': 400
        }, status=400, safe=False)

    try:
        date = request.GET['date']
        date = validate_date(date)
    except:
        date = str(datetime.date.today())
    try:
        if isinstance(date, JsonResponse):
            return date

        # Map provided agent id to musoni staff id
        agent_mapping = map_agent(agent_id)
        if isinstance(agent_mapping, dict):
            agent_id = agent_mapping['data']['identity']['staffId']
        else:
            return JsonResponse({
                'error': "Not found",
                "message": 'Agent not found',
                "error_description": 'Agent with that id does not exist',
                "status": 400
            }, safe=False, status=404)

        # Compute the current month to date
        # today = datetime.datetime.today().date()
        # start_of_month = datetime.datetime.today().date().replace(day=1)
        # logger.debug("start date: " + str(start_of_month))
        # ms = time()
        # rst_repayments = requests.get(
        #     settings.MUSONI_URL + quote('/runreports/Expected_Repayment'),
        #     auth=(settings.MUSONI_USER, settings.MUSONI_PWD),
        #     params={
        #         'R_endDate': today,
        #         'R_startDate': start_of_month,
        #         'R_environementUrl': settings.MUSONI_ENV_URL,
        #         'R_selectLoanofficer': agent_id,
        #         'R_currencyDigit': 1,
        #         'R_selectOffice': 1,
        #         'locale': 'en',
        #         'output-type': 'CSV',
        #     },
        #     headers={
        #         'X-Fineract-Platform-TenantId': 'tulaa'
        #     }
        # )

        # create a dynamic path to the collection data dump
        collections_path = os.path.join(BASE_DIR, 'data/extended_collections.csv')

        # convert the data from CSV to arraylist
        repayments = load_data(collections_path)
        # repayments = convert_txt_to_matrix(rst_repayments)

        # process the arraylist to get the required data
        res = loan_officer_collections_per_farmer(repayments, agent_id)
        return JsonResponse(res, safe=False)
    except:
        logger.error("An error occurred: " + str(traceback.format_exc()))
        return Response({
            'error': "Internal server error",
            "message": 'Internal server error',
            "error_description": 'Something went wrong. Contact support or try again later',
            "status": 500
        }, status=500)


# TODO: Delete this API as it's no longer in use
@api_view(['GET'])
@authentication_classes([TulaaBackend])
def loan_officer_history_view(request):
    """
          :param request: take in only one param `agentId: <uuid>`
          On success:
                   {
                     "farmers": [
                          {
                            "client_name": string,
                            "arrears": number,
                            "number": number,
                            "days_in_arrears": number,
                          }
                      ],
                  }

          on error:
                {
                  'error': string,
                  "message": string,
                  'error_description': string,
                  'status': number
              }
      """
    tt = time()
    try:
        agent_id = request.GET['agentId']
    except:
        settings.logger.error("An error occurred while getting agent id: " + str(traceback.format_exc()))
        return JsonResponse({
            'error': "Invalid parameter",
            "message": 'Invalid parameter "agentId"',
            'error_description': 'AgentId should be provided in the params',
            'status': 400
        }, status=400, safe=False)

    try:
        # Map provided agent id to musoni staff id
        agent_mapping = map_agent(agent_id)
        if isinstance(agent_mapping, dict):
            agent_id = agent_mapping['data']['identity']['staffId']
        else:
            return JsonResponse({
                'error': "Not found",
                "message": 'Agent not found',
                "error_description": 'Agent with that id does not exist',
                "status": 400
            }, status=404, safe=False)

        # create a dynamic path to the arrears status data dump
        path = os.path.join(BASE_DIR, 'data/arrears.csv')
        arrears = load_data(path)
        field_map = generate_field_mappings(FIELDS, arrears[0])

        agent = [r for r in arrears[1:] if r[field_map['firstloanofficerid']] == str(agent_id)]
        farmers = []
        # contains the farmers details
        client_data = load_data(settings.BASE_DIR + '/data/clients.csv')
        if len(agent) > 0:
            for record in agent:
                if record[field_map['firstloanofficerid']] != record[field_map['loanofficerid']]:
                    number = binary_search(client_data[1:], int(record[field_map['clientid']]), 1)
                    if isinstance(number, str):
                        number = ""
                    else:
                        number = number[4]
                    farmers.append({
                        "client_name": record[field_map['clientname']],
                        "arrears": record[field_map['arrearsamount']],
                        "number": number,
                        "days_in_arrears": record[field_map['arrearsdays']]
                    })
            if not farmers:
                farmers = {
                    "message": "No Loan History Available for this agent"
                }
            return JsonResponse(farmers, status=200, safe=False)

        result = {
            'message': "No data available for this agent"
        }
        logger.debug('time to taken to fetch loan history -> ', time() - tt)
        return JsonResponse(result, safe=False, status=404)
    except:
        logger.error("An error occurred: " + str(traceback.format_exc()))
        return Response({
            'error': "Internal server error",
            "message": 'Internal server error',
            "error_description": 'Something went wrong. Contact support or try again later',
            "status": 500
        }, status=500, safe=False)


@api_view(['GET'])
@authentication_classes([TulaaBackend])
def farmers_upcoming_repayments(request):
    """
        Part of the query params that should be provided are the start and end dates of interest, if no dates are
        provided it assumes that the query searches for the next 30 days to come from the current date
        This view provides the expected repayments that a loan officer should be receiving within the next 30 days.
        :param request:
        :return: {
            farmers: {
                name: string,
                expected_repayment: number,
                date: string,
            }
        }
    """

    try:
        agent_id = request.GET['agentId']
    except:
        settings.logger.error("An error occurred while getting agent id: " + str(traceback.format_exc()))
        return JsonResponse({
            'error': "Invalid parameter",
            "message": 'Invalid parameter "agentId"',
            'error_description': 'AgentId should be provided in the params',
            'status': 400
        }, status=400, safe=False)

    try:
        date = request.GET['date']
        date = validate_date(date)
    except:
        date = str(datetime.date.today())
        endDate = str(datetime.date.today() + datetime.timedelta(days=30))

    try:
        if isinstance(date, JsonResponse):
            return date
        agent_mapping = map_agent(agent_id)
        if isinstance(agent_mapping, dict):
            agent_id = agent_mapping['data']['identity']['staffId']
        else:
            return JsonResponse({
                'error': "Not found",
                "message": 'Agent not found',
                "error_description": 'Agent with that id does not exist',
                "status": 400
            }, status=404, safe=False)

        rst = requests.get(
            settings.MUSONI_URL + '/runreports/Expected_Repayment',
            auth=(settings.MUSONI_USER, settings.MUSONI_PWD),
            params={
                'R_selectOffice': 1,
                'R_startDate': date,
                'R_endDate': endDate,
                'R_environementUrl': settings.MUSONI_ENV_URL,
                'R_selectLoanofficer': agent_id,
                'R_currencyId': 1,
                'output-type': 'CSV',
                'locale': 'en'
            },
            headers={
                'X-Fineract-Platform-TenantId': 'tulaa'
            }
        )

        if rst.status_code == 200:
            # Some data manipulations here. To make sure the the data coming from the server is consistent
            # for processing
            repayments = convert_txt_to_matrix(rst)
            fields = [
                'Client Name:',
                'Client ID:',
                'Loan Status:',
                'Expected Repayment',
                'Amount Repaid',
                'Total Due',
                'Arrears'
            ]
            header = 7
            field_map = generate_field_mappings(fields, repayments[header])
            farmers = []
            client_data = read_from_pickle(settings.BASE_DIR + '/data/' + settings.CLIENT_DATA)
            cf = 0

            for record in repayments[header + 1:]:
                if record[field_map['clientid:']]:
                    number = binary_search(client_data[1:], int(record[field_map['clientid:']].replace('-', '')), 5)
                    if isinstance(number, str):
                        number = ""
                    else:
                        number = number[4]

                    if float(record[field_map['arrears']].replace(',', '')) > 0:
                        # Calculating the amount that is being carried forward due to prior unpaid amount. To achieve this
                        # check that arrears is greater than 0 and then using this formula
                        #               cf =  arrears - expected_repayments
                        # to derive the amount that is not repaid from the previous period

                        cf = float(record[field_map['arrears']].replace(',', '')) - float(
                            record[field_map['expectedrepayment']].replace(',', ''))

                    farmers.append({
                        'name': record[field_map['clientname:']],
                        'number': number,
                        'expected_repayment_date': record[field_map['expectedrepayment'] - 1],
                        'expected_repayment_amount': record[field_map['expectedrepayment']],
                        'carried_forward': cf,
                        'arrears': record[field_map['arrears']]
                    })
                else:
                    break
            if not farmers:
                farmers = {
                    'message': 'No records exists for that agent/period of interest.'
                }
            return JsonResponse(farmers, status=200, safe=False)

        return JsonResponse({
            'error': "Cannot be found",
            "message": 'Unable to fetch this data at the moment',
            'error_description': 'Musoni system maybe down or no content is available for this particular agent or time'
                                 ' frame',
            'status': 400
        }, status=400, safe=False)
    except:
        logger.error("An error occurred: " + str(traceback.format_exc()))
        return Response({
            'error': "Internal server error",
            "message": 'Internal server error',
            "error_description": 'Something went wrong. Contact support or try again later',
            "status": 500
        }, status=500, safe=False)


@api_view(['GET'])
@authentication_classes([TulaaBackend])
def loan_officer_commissions_view(request):
    """
    This endpoint should receive the agent's id as the only parameter
    It returns the repayments that have come in for a particular TB in that period of interest for their current active loans
    Challenge with this functionality is that Musoni does not currently provide any data on when a loan officer was assigned
    to a particular loans.
     As such the current implementation below, assumes that if a tulaabingwa is the current loan officer then they must
     have been assigned to the loan at the start of the month (logic behind this is that loans are graduated to different
     loan officers every 30 days and as such that should be relatively accurate.)

     The return should be
     {
        total_collections: '',
        commission; '',
     }
    """
    try:
        agent_id = request.GET['agentId']
    except:
        settings.logger.error("An error occurred while getting agent id: " + str(traceback.format_exc()))
        return JsonResponse({
            'error': "Invalid parameter",
            "message": 'Invalid parameter "agentId"',
            'error_description': 'AgentId should be provided in the params',
            'status': 400
        }, status=400, safe=False)

    try:
        agent_mapping = map_agent(agent_id)
        if isinstance(agent_mapping, dict):
            agent_id = agent_mapping['data']['identity']['staffId']
        else:
            return JsonResponse({
                'error': "Not found",
                "message": 'Agent not found',
                "error_description": 'Agent with that id does not exist',
                "status": 400
            }, status=404, safe=False)
        path = os.path.join(BASE_DIR, 'data/arrears.csv')
        arrears = load_data(path)
        field_map = generate_field_mappings(FIELDS, arrears[0])

        agent = [r for r in arrears[1:] if r[field_map['firstloanofficerid']] == str(agent_id)]

        # Compute the current month to date
        today = datetime.datetime.today().date()
        start_of_month = datetime.datetime.today().date().replace(day=1)
        # fetch the loan officer collections reports
        rst = requests.get(
            settings.MUSONI_URL + '/runreports/Expected_Repayment',
            auth=(settings.MUSONI_USER, settings.MUSONI_PWD),
            params={
                'R_selectOffice': 1,
                'R_startDate': start_of_month,
                'R_endDate': today,
                'R_environementUrl': settings.MUSONI_ENV_URL,
                'R_selectLoanofficer': agent_id,
                'R_currencyId': 1,
                'output-type': 'CSV',
                'locale': 'en'
            },
            headers={
                'X-Fineract-Platform-TenantId': 'tulaa'
            }
        )

        if rst.status_code == 200:
            # Some data manipulations here. To make sure the the data coming from the server is consistent
            # for processing
            repayments = convert_txt_to_matrix(rst)
            fields = [
                'Client Name:',
                'Client ID:',
                'Loan Status:',
                'Expected Repayment',
                'Amount Repaid',
                'Total Due',
                'Arrears'
            ]
            header = 7
            field_map = generate_field_mappings(fields, repayments[header])
            amount_repaid = 0
            for record in repayments[header + 1:]:
                if record[field_map['clientid:']]:
                    amount_repaid += float(record[field_map['amountrepaid']].replace(',', ''))
                else:
                    break

            result = {
                'amount_collected': amount_repaid,
                'commission': round(amount_repaid * settings.COMMISSION, 2)
            }
            return JsonResponse(result, status=200, safe=False)

        return JsonResponse({
            'error': "Cannot be found",
            "message": 'Unable to fetch this data at the moment',
            'error_description': 'Musoni system maybe down or no content is available for this particular agent or time'
                                 ' frame',
            'status': 400
        }, status=400, safe=False)
    except:
        logger.error("An error occurred: " + str(traceback.format_exc()))
        return Response({
            'error': "Internal server error",
            "message": 'Internal server error',
            "error_description": 'Something went wrong. Contact support or try again later',
            "status": 500
        }, status=500, safe=False)

# farmers = []
# if len(agent) > 0:
#     for record in agent:
#         # Fetch the transaction history of each loan and perform the filter assuming
#         # Assuming tht at the start of the month is when the loan officer was assigned
#         res = requests.get(
#             "{}/loans/{}".format(settings.MUSONI_URL, record[field_map['accountno']]),
#             auth=(settings.MUSONI_USER, settings.MUSONI_PWD),
#             params={
#                 "associations": 'transactions'
#             },
#             headers={
#                 'X-Fineract-Platform-TenantId': 'tulaa'
#             }
#         )
#         loan = json.loads(res.content.decode('utf-8'))
#         print(loan)
