import threading

from django.apps import AppConfig
from auth_custom import authentication
from musoni import settings
import traceback

logger = settings.logger


class TulaabingwaConfig(AppConfig):
    name = 'tulaabingwa'

    def ready(self):
        try:
            authentication.authenticate()
            from utils import client_data
            client_data.download_client_data()
            logger.debug("importing scheduler")
            from scheduler import data_export_downloader
            data_export_downloader.download_data_export()
            data_export_downloader.download_collections()
            from dashboards import loan_schedule
            loan_schedule.fetch_loan_schedule()
            from zoho import zoho
            logger.debug("creating farmers in background")
            # zoho.create_farmers_background()
            logger.debug("starting service")
            # zoho.bulk_create_ticket()
            # run the scheduler in the background
            data_export_downloader.start()
        except:
            logger.error("An error occurred: " + str(traceback.format_exc()))
