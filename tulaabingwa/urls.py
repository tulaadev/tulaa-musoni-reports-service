from django.urls import path
from .views import arrears_status_view, collections_view, loan_officer_history_view, farmers_upcoming_repayments, \
    loan_officer_commissions_view

urlpatterns = [
    path('arrears', arrears_status_view),
    path('collections', collections_view),
    path('history', loan_officer_history_view),
    path('expected_repayments', farmers_upcoming_repayments),
    path('commission', loan_officer_commissions_view)
]

