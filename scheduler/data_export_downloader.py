"""
This file contains the entry point of the scheduler. The scheduler here is implemented using the
apscheduler package. The timings can be adjusted using cron-based triggers, intervals or other events.
Check out the documentation on this library here https://apscheduler.readthedocs.io/en/stable/
"""
import logging

from apscheduler.schedulers.background import BackgroundScheduler
import requests
import datetime

from typing import List, Any
from urllib.parse import quote
import traceback
from dashboards.loan_schedule import fetch_loan_schedule
from musoni import settings
from auth_custom import authentication
from utils import client_data, CSV_helper
from utils.binary_search import binary_search
from utils.helpers import load_data
from utils.quick_sort import quick_sort
from zoho import zoho

from zoho.cleaning_zoho_data import start_delete

logger = settings.logger
arrears_data = []


def transform_row(line, line_num):
    # if this is the first line, append the header
    # if 72 < line_num < 77:
    #     logger.debug("line " + str(line_num))
    #     logger.debug("client data len: " + str(len(client_data)))
    #     logger.debug("client ID: " + str(line[3]))
    #     logger.debug(int(line[3]))

    # logger.debug("line data " + str(line))
    if line_num == 1:
        line.append("loan_officer_id")
        line.append("arrears")
    else:
        # look for the  loan officer in the arrears data
        client_more_data = binary_search(arrears_data[1:], int(line[2]),
                                         9)  # 2 is the account number in collections, 9 is the acc no  in arrears
        if client_more_data == 'Not found':
            line.append(client_more_data)
            line.append("")
        else:
            line.append(str(client_more_data[10]))
            line.append(str(client_more_data[6]))


def download_data_export():
    try:
        """
    Downloads the loan arrears data export setup in Musoni and stores in the `data/` directory
    :return:  None
    """
        url = settings.MUSONI_URL + quote(settings.MUSONI_LOAN_ARREARS)
        logger.debug('Fetching the data from Musoni')

        data = requests.get(
            url,
            auth=(settings.MUSONI_USER, settings.MUSONI_PWD),
            headers={
                'X-Fineract-Platform-TenantId': 'tulaa'
            }
        )
        # datar = data.content.decode('utf-8').split('\r')
        dataa = data.content.decode('utf-8').split('\r\n')
        # with open(settings.BASE_DIR + '/data/arrears.csv', 'w+', encoding='utf-8', newline='') as f:
        #     for row in datar:
        #         f.write(row)

        # save a sorted one
        data_list = []
        # count = 0

        for row in dataa:
            data_list.append(row.split(';'))
        #     if count < 3:
        #         logger.debug(row)
        #         logger.debug(data_list[count])
        #         count += 1
        # logger.debug(data_list[len(data_list) - 1])

        # remove the last item as it is invalid
        data_list.pop()
        quick_sort(data_list, 1, len(data_list) - 1, 9)

        with open(settings.BASE_DIR + '/data/arrears.csv', 'w+', encoding='utf-8', newline='') as f:
            for row in data_list:
                f.write(';'.join(row) + '\r')

        # logger.debug("saved to ->", settings.BASE_DIR + '/data/arrears.csv')
        logger.debug('Fetch complete')
    except:
        logger.error("an error occurred while getting arrears: " + str(traceback.format_exc()))


def download_collections():
    try:
        url = settings.MUSONI_URL + quote(settings.MUSONI_COLLECTIONS)
        logger.debug('Fetching the collections data from Musoni')

        data = requests.get(
            url,
            auth=(settings.MUSONI_USER, settings.MUSONI_PWD),
            headers={
                'X-Fineract-Platform-TenantId': 'tulaa'
            }
        )
        # put this data in an array of lists
        # sort the array
        # write it to file

        arr = []
        for row in data.content.decode('utf-8').split('\r\n'):
            arr.append(row.split(';'))
        # logger.debug("dumping the content to a data dump")
        arr.pop()

        with open(settings.BASE_DIR + '/data/collections.csv', 'w', encoding='utf-8', newline='') as f:
            for row in arr:
                f.write(';'.join(row) + '\r')

        global arrears_data
        arrears_data = load_data(settings.BASE_DIR + '/data/arrears.csv')  # type: List[Any]
        # now modify the file to add first loan officer column
        CSV_helper.add_column_in_csv_reusable(settings.BASE_DIR + '/data/collections.csv',
                                              settings.BASE_DIR + '/data/extended_collections.csv', transform_row)
    except:
        logger.error("An error occurred while processing the loan schedule data: " + str(traceback.format_exc()))


def start():
    """
    The entry function for apsscheduler scheduler
    :return: None
    """
    scheduler = BackgroundScheduler()
    scheduler.add_job(download_data_export, 'interval', minutes=50)
    scheduler.add_job(client_data.download_client_data, 'interval', minutes=50)
    scheduler.add_job(download_collections, 'interval', minutes=50)
    scheduler.add_job(authentication.authenticate, 'interval', minutes=10)
    scheduler.add_job(zoho.bulk_create_farmer, 'interval', days=1)
    # scheduler.add_job(zoho.bulk_create_ticket, 'interval', days=1)

    # scheduler.add_job(start_delete, 'interval', minutes=0.5)git
    scheduler.add_job(fetch_loan_schedule, 'interval', minutes=55)
    scheduler.start()
