"""
This file contains functions that perform against the agent mapping service.
"""
import json
import requests
from musoni import settings
import pickle


def authenticate():
    try:
        url = settings.UAA_SERVER_URL
        token = requests.post(
        url,
        data={
            "grant_type": "client_credentials"
        },
        auth=(settings.CLIENT_ID, settings.CLIENT_SECRET),
        # auth=('musoni-reports',  '353cf5cd-fbb2-46cb-a8e2-3b2815ec2232'),
        headers={
            "Content-Type": "application/x-www-form-urlencoded"
        }
        )
        if token.status_code == 200:
            dump_to_pickle(json.loads(token.content.decode('utf-8'))['access_token'], settings.PICKLE_NAME)
            settings.logger.debug("Fetched and saved the user token.")
            return token.content.decode('utf-8')
        else:
            settings.logger.error("Unable to fetch mapping token, some functions may not work as expected. Status code: " + str(token.status_code) + "Message: " + token.content.decode('utf-8'))
    except Exception as e:
        settings.logger.error("an error occurred (raised internally): " + str(e))
        #raise


def dump_to_pickle(data, location):
    outfile = open(location, 'wb')
    pickle.dump(data, outfile)
    outfile.close()


def read_from_pickle(location):
    infile = open(location, 'rb')
    token = pickle.load(infile)
    infile.close()
    return token
