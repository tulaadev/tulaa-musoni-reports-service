"""
A custom authentication backend that handles all authentication functions.
It handles both authentication schemes used at Tulaa ie. oauth and JWT tokens
"""
from django.contrib.auth.models import AnonymousUser
from django.http import JsonResponse
from django.utils.translation import gettext_lazy as _
from rest_framework.authentication import BaseAuthentication
from rest_framework import exceptions
from musoni import settings
import jwt
from jwt.contrib.algorithms.pycrypto import RSAAlgorithm
from jwt.exceptions import ExpiredSignatureError
from rest_framework import status
import requests

OAUTH_VERIFY_URL = settings.OAUTH_VERIFY_URL
logger = settings.logger
SECRET = 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAum6PQ7YgAxZ3QflF1SekwPbzuGhx4DxlYbaqghPOSm1mPcGSmPRonYvGI/gA6cv6+PmEt/fuUPXv8Y/cu+RGvwSK1z82NwRD5xpxaKcrKhafUkx6r98LfPCIcq25dVKyRSYerBG8P8urcLsljihI3QjRhL8PlPIIxfLj2YXrcCLRcKtZpWfN7fiFyt7cQ2BnJ2lScR+cOKeSE7dfZyo1QYGd9fMB1M+uIyZ3LzDg4OFCHBsg8H4zHo4SBB0iYIeYk5zvl+MxCIwQc9VBtSFr3PijVAJkdEAk9+pDXcGD3CBud4qSKoxRQR5fQ6Tlv5rzeIdE/z/xZXrYoMjHI1DRPQIDAQAB'
SECRET_PEM = '-----BEGIN PUBLIC KEY-----\n' + SECRET + '\n-----END PUBLIC KEY-----'


class CustomException(exceptions.APIException):
    status_code = status.HTTP_401_UNAUTHORIZED
    default_detail = _('Incorrect authentication credentials.')
    default_code = 'error'


class TulaaBackend(BaseAuthentication):
    def authenticate(self, request, username=None, password=None, **kwargs):
        jwt.unregister_algorithm('RS256')
        jwt.register_algorithm('RS256', RSAAlgorithm(RSAAlgorithm.SHA256))
        try:
            authorization = request.headers['Authorization']
        except:
            raise CustomException(
                {
                    'error': 'missing_token',
                    'error_description': 'Authorization Credentials missing'
                }
            )

        if authorization:
            bearer, token = request.headers['Authorization'].split(' ')

            if len(token.split('.')) == 1:
                # perform an oauth authentication
                res = requests.get(OAUTH_VERIFY_URL,
                                   auth=(
                                       settings.CLIENT_ID,
                                       settings.CLIENT_SECRET),
                                   params={
                                       'token': token
                                   },
                                   headers={})
                if res.status_code != 200:
                    raise CustomException(
                        {
                            'error': 'invalid_token',
                            'error_description': 'Could not authenticate, Invalid Credentials'
                        }
                    )
                else:
                    return AnonymousUser, None
            else:
                # Decode the JWT token and confirm that the jwt key is valid
                try:
                    decrypted = jwt.decode(token, SECRET_PEM,
                                           algorithms=['RS256'],
                                           audience='tulaa-accesscontrol')
                    return AnonymousUser, None
                except ExpiredSignatureError:
                    raise CustomException({
                        'error_description': "Invalid Credentials, Expired Signature",
                        'error': 'invalid_token'
                    },
                    )

        raise CustomException({
            'error': 'invalid_token',
            'error_description': 'Could not authenticate, Invalid Credentials'
        })
