"""
Classes ZohoDesk, HttpCurlUtil, Zoho Agents reside here
The file mainly contains utility function used in `zoho.py` file
which is the main base for the functionality.
A brief summary of the core classes:
- HttpsCURLUtils: a class that implements some basic curl/request calls class
- ZohoDesk : Maps most of the api endpoint used into functions
- ZohoAgent: special class that maps out Zoho Agents apis in zoho
"""
import json
import time
from datetime import datetime
from urllib.parse import urlparse, urlencode, urlunparse
from urllib.request import HTTPSHandler, Request
from musoni import settings
logger = settings.logger


class HttpsCURLUtils(HTTPSHandler):
    def __init__(self, base_url='', default_headers={}, timeout=300):
        self.base_url = base_url
        self.default_headers = default_headers
        self.timeout = timeout
        super(HttpsCURLUtils, self).__init__()

    def _build_url(self, path, args_dict={}):
        # Returns a list in the structure of urlparse.ParseResult
        url_parts = list(urlparse(self.base_url))
        # TODO : merge path  manage '/' assuming no '/' in base url and '/' in path
        url_parts[2] = str(url_parts[2]) + path
        if args_dict:
            url_parts[4] = urlencode(args_dict)
        return urlunparse(url_parts)

    def _send(self, method='GET', url=None, headers={}, data=None):
        req = Request(url=url, headers=headers, method=method, data=data)
        req.timeout = self.timeout
        return self.https_open(req)

    def get(self, path=None, query_params={}, headers={}):
        url = self._build_url(path, query_params)
        headers.update(self.default_headers)
        return self._send(method='GET', url=url, headers=headers)

    def post(self, path=None, data={}, headers={}):
        url = self._build_url(path)
        headers.update(self.default_headers)
        if data and isinstance(data, dict):
            data = json.dumps(data)
        return self._send(method='POST', url=url, headers=headers, data=data)

    def patch(self, path=None, data={}, headers={}):
        url = self._build_url(path)
        headers.update(self.default_headers)
        if data and isinstance(data, dict):
            data = json.dumps(data)
        return self._send(method="PATCH", url=url, headers=headers, data=data)


class ZohoDeskException(Exception):
    def __init__(self, message='', curl_res=None):
        data = None
        if curl_res:
            data = curl_res.read()
        if data:
            data = data.decode("utf-8")
        self._desk_error = message + " Zoho ErrorCode <{}> {}".format(str(curl_res.status), data)
        super().__init__(self._desk_error)


class ZohoDesk:
    def __init__(self, zoho_auth_token, orgid):
        self.zoho_desk_base_url = settings.ZOHO_URL

        self.zoho_auth_token = zoho_auth_token
        self.orgid = orgid
        self.curl = HttpsCURLUtils(self.zoho_desk_base_url)

        self.headers = {
            'Authorization': 'Zoho-authtoken ' + self.zoho_auth_token,
            'orgId': self.orgid
        }

    def reset_session(self):
        self.curl = HttpsCURLUtils(self.zoho_desk_base_url)

    def _process_data(self, curl_res):
        data = curl_res.read()
        data = data.decode("utf-8")
        if not data:
            return None
        data = json.loads(data)
        return data

    def create_ticket(self, data={}, *args, **kwargs):
        url = '/api/v1/tickets'
        self.headers['Content-Type'] = "application/json"
        curl_res = self.curl.post(url, data=data, headers=self.headers)
        if str(curl_res.status) in ['200']:
            data = self._process_data(curl_res)
            return data
        else:
            logger.error('Cant create Ticket'+  str(curl_res))
            logger.error('Cant create Ticket (cred)'+  str(curl_res.read().decode("utf-8")))

    def delete_tickets(self, data={}, *args, **kwargs):
        url = '/api/v1/tickets/moveToTrash'
        self.headers['Content-Type'] = "application/json"
        curl_res = self.curl.post(url, data=data, headers=self.headers)
        if str(curl_res.status) in ['200']:
            data = self._process_data(curl_res)
            return data
        else:
            raise ZohoDeskException('Cant delete Tickets', curl_res)

    def get_ticket(self, query_params):
        url = '/api/v1/tickets/search'
        curl_res = self.curl.get(url, query_params=query_params, headers=self.headers)
        if str(curl_res.status) in ['200', '204']:
            data = self._process_data(curl_res)
            return data
        else:
            raise ZohoDeskException("cant get ticket", curl_res)

    def update_ticket(self, id, data):
        url = '/api/v1/tickets/{}'.format(id)
        self.headers['Content-Type'] = "application/json"
        curl_res = self.curl.patch(url, data=data, headers=self.headers)
        if str(curl_res.status) == '200':
            data = self._process_data(curl_res)
            return data
        else:
            logger.error('Cant not update zoho ticket (curlread).' + str(curl_res.read().decode("utf-8")))
            return None

    def get_contact(self, query_params={}):
        url = '/api/v1/contacts/search'
        curl_res = self.curl.get(url, query_params=query_params, headers=self.headers)
        if str(curl_res.status) == '200':
            data = self._process_data(curl_res)
            return data["data"][0] if data["data"] else None

    def create_contact(self, data):
        url = '/api/v1/contacts'
        self.headers['Content-Type'] = "application/json"
        curl_res = self.curl.post(url, data=data, headers=self.headers)
        if str(curl_res.status) == '200':
            data = self._process_data(curl_res)
            return data
        else:
            raise ZohoDeskException('Cant not create zoho user.', curl_res)

    def create_agent(self, data):
        url = '/api/v1/agents'
        self.headers['Content-Type'] = "application/json"
        curl_res = self.curl.post(url, data=data, headers=self.headers)
        if str(curl_res.status) == '200':
            data = self._process_data(curl_res)

            return data
        else:
            raise ZohoDeskException('Cant not create zoho user.', curl_res)

    def update_contact(self, id, data):
        url = '/api/v1/contacts/{}'.format(id)
        self.headers['Content-Type'] = "application/json"
        curl_res = self.curl.patch(url, data=data, headers=self.headers)
        if str(curl_res.status) == '200':
            data = self._process_data(curl_res)
            return data
        else:
            raise ZohoDeskException('Cant not update zoho user.', curl_res)

    def get_agents(self, query_params={}):
        url = '/api/v1/agents'.format(id)
        self.headers['Content-Type'] = "application/json"
        curl_res = self.curl.get(url, query_params=query_params, headers=self.headers)
        if str(curl_res.status) == '200':
            data = self._process_data(curl_res)
            return data
        else:
            raise ZohoDeskException('Cant not get zoho agents.', curl_res)


class ZohoAgents:
    def _load_agent(self, *args, **kwargs):
        desk = ZohoDesk(
            zoho_auth_token=kwargs['zoho_auth_token'],
            orgid=kwargs['orgid'],
        )
        data = desk.get_agents(query_params={
            "limit": 200
        })
        return data.get('data', [])

    def __init__(self, *args, **kwargs):
        self._agent_list = self._load_agent(*args, **kwargs)
        # filter 'isConfirmed'
        self.agent_list = [a for a in self._agent_list if
                           a.get('isConfirmed')]
        self.index = -1

    def __iter__(self):
        return self

    def __next__(self):
        self.index += 1
        return self.agent_list[self.index % len(self.agent_list)]

    def get_agent_by_name(self, name):
        for a in self.agent_list:
            name = name.replace(" ", "")
            a_name = str(a['firstName'] + a['lastName']).replace(" ", "")
            if name == a_name:
                return a
        return next(self)

    def get_agent_by_email(self, email):
        for a in self._agent_list:
            email = email.lower()
            if email == a['emailId']:
                return a
        return next(self)

    def get_agent_by_role_id(self, roleId):
        agents = []
        for a in self.agent_list:
            roleId = roleId.lower()
            if roleId == a['roleId']:
                agents.append(a)
        return agents


class TulaaBingwa:
    def __init__(self, tb):
        self.tb = tb
        self.index = -1

    def __iter__(self):
        return self

    def __next__(self):
        self.index += 1
        return self.tb[self.index % len(self.tb)]


class TrackTime:
    def __init__(self):
        self.start_time = time.time()
        self.start_datetime = datetime.now()

    def time_passed(self):
        delta = time.time() - self.start_time
        print("Executed in {}s".format(delta))
        return self.start_datetime, delta, datetime.now()


def parse_currency_to_float(value):
    try:
        value = str(value).replace(",", "")
        return float(value)
    except:
        return float(0)


def get_zoho_contact_by_farmer_id(desk, farmer_id):
    """

    :param desk: ZohoDesk
    :param farmer_ID:
    :return:
    """
    return desk.get_contact(query_params={
        "customField1": "cf_farmer_id:{}".format(farmer_id)
    })
