from django.db import models


class ZohoTb(models.Model):
    musoni_loan_officer_id = models.CharField('Musoni TB id', max_length=10, db_column='musoni_loan_officer_id')
    zoho_tb_id = models.CharField('Zoho TB id', max_length=10, db_column='zoho_agent_id')

    class Meta:
        db_table = "zoho_musoni_mapping"

    def _str_(self):
        return self.name

    @staticmethod
    def get_agent_mapping(musoni_id):
        try:
            zoho_id = ZohoTb.objects.get(musoni_loan_officer_id=musoni_id).zoho_tb_id
            return zoho_id
        except ZohoTb.DoesNotExist:
            return None

    @staticmethod
    def create_tb_mapping(musoni_id, zoho_id):
        try:
            ZohoTb.objects.create(zoho_tb_id=zoho_id, musoni_loan_officer_id=musoni_id)
        except:
            raise


class TbMusoniMapping(models.Model):
    user_id = models.CharField('user_id', max_length=50, db_column='user_id', primary_key=True)
    staff_id = models.CharField('loan_officer_id', max_length=50, db_column='staff_id')

    class Meta:
        db_table = 'musoni_loan_officer'

    def _str_(self):
        return self.name

    @staticmethod
    def get_agent_id(musoni_id):
        try:
            agent_id = TbMusoniMapping.objects.get(staff_id=musoni_id).user_id
            return agent_id
        except TbMusoniMapping.DoesNotExist:
            return None


class TbUserDetails(models.Model):
    id = models.CharField('user id', max_length=50, primary_key=True)
    email = models.CharField('email', max_length=50)

    class Meta:
        db_table = 'users'

    def _str_(self):
        return self.name

    @staticmethod
    def get_agent_email(agent_id):
        try:
            email = TbUserDetails.objects.get(id=agent_id).email
            return email
        except TbUserDetails.DoesNotExist:
            return None
