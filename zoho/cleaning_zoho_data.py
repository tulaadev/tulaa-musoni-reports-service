import json
from urllib.parse import urlparse, urlencode, urlunparse
from urllib.request import HTTPSHandler, Request

from musoni import settings

ZOHO_DESK_AUTH_TOKEN = 'ec17aa41eac7a464380b76519e021b05'
ZOHO_DESK_ORG_ID = '695221010'


class HttpsCURLUtils(HTTPSHandler):
    def __init__(self, base_url='', default_headers={}, timeout=300):
        self.base_url = base_url
        self.default_headers = default_headers
        self.timeout = timeout
        super(HttpsCURLUtils, self).__init__()

    def _build_url(self, path, args_dict={}):
        # Returns a list in the structure of urlparse.ParseResult
        url_parts = list(urlparse(self.base_url))
        # TODO : merge path  manage '/' assuming no '/' in base url and '/' in path
        url_parts[2] = str(url_parts[2]) + path
        if args_dict:
            url_parts[4] = urlencode(args_dict)
        return urlunparse(url_parts)

    def _send(self, method='GET', url=None, headers={}, data=None):
        req = Request(url=url, headers=headers, method=method, data=data)
        req.timeout = self.timeout
        return self.https_open(req)

    def get(self, path=None, query_params={}, headers={}):
        url = self._build_url(path, query_params)
        headers.update(self.default_headers)
        return self._send(method='GET', url=url, headers=headers)

    def post(self, path=None, data={}, headers={}):
        url = self._build_url(path)
        headers.update(self.default_headers)
        if data and isinstance(data, dict):
            data = json.dumps(data)
        return self._send(method='POST', url=url, headers=headers, data=data)

    def patch(self, path=None, data={}, headers={}):
        url = self._build_url(path)
        headers.update(self.default_headers)
        if data and isinstance(data, dict):
            data = json.dumps(data)
        return self._send(method="PATCH", url=url, headers=headers, data=data)


class ZohoDeskException(Exception):
    def __init__(self, message='', curl_res=None):
        data = None
        if curl_res:
            data = curl_res.read()
        if data:
            data = data.decode("utf-8")
        self._desk_error = message + " Zoho ErrorCode <{}> {}".format(str(curl_res.status), data)
        super().__init__(self._desk_error)


class ZohoDesk:
    def __init__(self, zoho_auth_token, orgid):
        self.zoho_desk_base_url = 'https://desk.zoho.com'

        self.zoho_auth_token = zoho_auth_token
        self.orgid = orgid
        self.curl = HttpsCURLUtils(self.zoho_desk_base_url)

        self.headers = {
            'Authorization': 'Zoho-authtoken ' + self.zoho_auth_token,
            'orgId': self.orgid
        }

    def reset_session(self):
        self.curl = HttpsCURLUtils(self.zoho_desk_base_url)

    def _process_data(self, curl_res):
        data = curl_res.read()
        data = data.decode("utf-8")
        if not data:
            return None
        data = json.loads(data)
        return data

    def create_ticket(self, data={}, *args, **kwargs):
        url = '/api/v1/tickets'
        self.headers['Content-Type'] = "application/json"
        curl_res = self.curl.post(url, data=data, headers=self.headers)
        if str(curl_res.status) in ['200']:
            data = self._process_data(curl_res)
            return data
        else:
            raise ZohoDeskException('Cant create Ticket', curl_res)

    def delete_tickets(self, data={}, *args, **kwargs):
        url = '/api/v1/tickets/moveToTrash'
        self.headers['Content-Type'] = "application/json"
        curl_res = self.curl.post(url, data=data, headers=self.headers)
        if str(curl_res.status) in ['200', '204']:
            data = self._process_data(curl_res)
            return data
        else:
            raise ZohoDeskException('Cant delete Tickets', curl_res)

    def delete_contacts(self, data):
        url = '/api/v1/contacts/moveToTrash'
        self.headers['Content-Type'] = "application/json"
        curl_res = self.curl.post(url, data=data, headers=self.headers)
        if str(curl_res.status) in ['200', '204']:
            data = self._process_data(curl_res)
            return data
        else:
            raise ZohoDeskException('Cant delete zoho user.', curl_res)

    def get_all_tickets(self, query_params):
        url = '/api/v1/tickets'
        curl_res = self.curl.get(url, query_params=query_params, headers=self.headers)
        if str(curl_res.status) in ['200', '204']:
            data = self._process_data(curl_res)
            return data
        else:
            raise ZohoDeskException("cant get ticket", curl_res)

    def get_all_contacts(self, query_params={}):
        url = '/api/v1/contacts'
        curl_res = self.curl.get(url, query_params=query_params, headers=self.headers)
        if str(curl_res.status) == '200':
            data = self._process_data(curl_res)
            return data["data"] if data["data"] else None


zoho_desk_api = ZohoDesk(settings.ZOHO_DESK_AUTH_TOKEN, settings.ZOHO_DESK_ORG_ID)


def clean_all_tickets(data):
    res = zoho_desk_api.delete_tickets(data)
    return "Delete all data {}".format(res)


def clean_all_contacts(data):
    res = zoho_desk_api.delete_contacts(data)
    return "Delete all contacts"


def tickets_delete():
    print("starting")
    tickets = zoho_desk_api.get_all_tickets({
        'limit': 50
    })
    ids = []
    if not tickets:
        return
    for ticket in tickets['data']:
        ids.append(ticket['id'])
    # import pdb; pdb.set_trace()
    print("length {} keys {} ".format(len(ids), ids))
    print(clean_all_tickets({
        'ticketIds': ids
    }))


def contacts_delete():
    contact_ids = []
    # import pdb; pdb.set_trace()
    contacts = zoho_desk_api.get_all_contacts({'limit': 50})
    if not contacts:
        return
    for contact in contacts:
        contact_ids.append(contact['id'])
    print("length {} keys {} ".format(len(contact_ids), contact_ids))
    print(clean_all_contacts({
        'contactIds': contact_ids
    }))


def start_delete():
    # contacts_delete()
    tickets_delete()


if __name__ == '__main__':
    """
    Fetch all tickets and iteratively delete over the whole set from Zoho API
    """
    print("starting")
    start_delete()
    # clean_all_tickets({
    #     'ticketIds': ids
    # })
