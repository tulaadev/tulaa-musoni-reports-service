"""
JIRA TICKET - https://tulaatech.atlassian.net/browse/PLATFORM-787
"""
import threading
import traceback
import time

from auth_custom.authentication import read_from_pickle
from musoni import settings
from utils.agent_collections import get_last_loan_repayment_date_with_amount
from utils.helpers import generate_field_mappings, load_data
from zoho.models import ZohoTb, TbMusoniMapping, TbUserDetails
from zoho.zoho_utils import ZohoDesk, ZohoAgents

zoho_desk_api = ZohoDesk(settings.ZOHO_DESK_AUTH_TOKEN, settings.ZOHO_DESK_ORG_ID)
# client_data = load_data(settings.BASE_DIR + '/data/clients.csv')
AGENTS = None
logger = settings.logger


def bulk_create_farmer():
  try:
    # Read Client Data
    client_data = read_from_pickle(settings.BASE_DIR + '/data/' + settings.CLIENT_DATA)
    fields = [
        "Client Name",
        "Client Id",
        "Date Of Birth",
        "Gender",
        "Phone Number",
        "Account No"
    ]
    field_map = generate_field_mappings(fields, client_data[0])
    count = 0
    # Now create the farmer body:
    for row in client_data[1:]:
      try:
        # To ensure that we dont exceed the API call limits for zoho
        if count % 100 == 0:
            logger.debug("Zoho is taking a rest")
            time.sleep(15)

        first_name, *last_name = row[field_map['clientname']].split(' ')
        farmer = {
            'firstName': first_name,
            'lastName': " ".join(last_name),
            'phone': row[field_map['phonenumber']],
            'cf': {
                'cf_farmer_id': row[field_map['clientid']],
                'cf_gender': row[field_map['gender']],
            }
        }
        # Perform an update or create
        # first search for that particular contact
        existing_contact = zoho_desk_api.get_contact({
            "customField1": "cf_farmer_id:${" + (row[field_map['clientid']]) + "}"
        })
        if existing_contact:
            #  perform the update on the contact
            # TODO: Ensure the right info according to the requirements is being updated.
            res = zoho_desk_api.update_contact(existing_contact.get('id'), farmer)
        else:
            res = create_farmer(farmer)
      except:
           logger.debug("Processing this farmer failed.  " + str(traceback.format_exc()))
    logger.debug("farmer contacts created or updated")
    bulk_create_ticket()
  except:
       logger.debug("Error at bulk create farmers: " + str(traceback.format_exc()))


def get_ticket(desk, cf_loan_id, cf_farmer_id):
    tickets = desk.get_ticket({
        "customField1": "cf_farmer_id:{}".format(cf_farmer_id),
        "customField2": "cf_loan_id:{}".format(cf_loan_id)
    })
    if not tickets:
        return None
    return tickets['data'][0] if tickets.get('data') else None

FIELDS = [
    'Loan Officer Id',
    'Loan Officer Name',
    'Client Id',
    'Client Name',
    'Phone Number',
    'Principal Amount',
    'Arrears Days',
    'Arrears Amount',
    'Principal Repaid Derived',
    'Principal Outstanding Derived',
    'Arrears Date',
    'Total Repayment Derived',
    'Loan Status Id',
    'Account No',
    'Disbursed On Date'
]
#simon.mulinge@tulaa.io
tickets = load_data(settings.BASE_DIR + '/data/arrears.csv')
field_map = generate_field_mappings(FIELDS, tickets[0])


def get_tb_email(musoni_id):
    # get agent ID
    agent_id = TbMusoniMapping.get_agent_id(musoni_id)
    # get agent email.
    if agent_id:
        logger.debug("agent_id: " + str(agent_id))
        email = TbUserDetails.get_agent_email(agent_id)
        return email
    else:
        return None


def create_agent(ticket):
    # create an agent in Zoho to map the same agent at Musoni
    last_name =  str(ticket[field_map['loanofficername']]).replace(",","").split(" ")[0]
    first_name = str(ticket[field_map['loanofficername']]).split(" ")[1]
    email =  get_tb_email(str(ticket[field_map['loanofficerid']]))

    # some names are empty. In that case, put Default then it will be corrected at Zoho interface
    if last_name == '':
        last_name = 'Default'
    if first_name == '':
        first_name = 'Default'
    zoho_agent = {
        "lastName" : last_name,
        "firstName" : first_name,
        #"countryCode" : "en_US",
        #"langCode" : "en_US",
        "rolePermissionType" : "AgentPublic",
        "emailId" : email, # str(ticket[field_map['loanofficername']]).replace(",","").split(" ")[0]+ '.' + str(ticket[field_map['loanofficername']]).split(" ")[1]+str(ticket[field_map['loanofficerid']])+'@tulaa.io',
        "associatedDepartmentIds" : ["424060000000164117", "424060000000006907"]
    }
    # if the email is None, don't bother creating. it won't work
    if email:
        new_zoho_agent = zoho_desk_api.create_agent(zoho_agent)
        return new_zoho_agent
    else:
        logger.debug("We can't create this agent due to invalid email")
        # check for this error code: THRESHOLD_EXCEED, or 429 at http error code level
        return None

def bulk_create_ticket():
    """
    Reads the arrears data for each farmer and then based on the number of days in arrears it decides what to assign.
    It reads from the arrears.csv file
    A ticket will contain the following details:
    {
        ticket_name =  farmer_name + days_in_arrears
        days_in_arrears = arrears,
        assigned_agent_id =  agentId
    }
    :return: None
    """
    

    # TODO: I may not need this part of loading ALL AGENTS since I will deal with the the agent attached to this ticket already. I am not doing new assigments
    # prepare_agent_list()
    # assert AGENTS, 'Agents required!'
    # logger.debug(AGENTS)
    # tulaa_bingwas = TulaaBingwa(AGENTS.get_agent_by_role_id('424060000003008085'))


    """
      Action rules for new tickets:
      Arrears < 30: Assign to agent, Action  Required - "Call A farmer"
      Arrears>30: Assign to 'supervisor' (annjeddy@tulaa.io), Action Required - "Visit Farmer"
      Arrears>60: Assign to 'supervisor' (annjeddy@tulaa.io), Action Required - "Visit Farmer"
      Arrears>90: Assign to 'supervisor' (annjeddy@tulaa.io), Action Required - "Visit Farmer"
    """
    # cf_follow_action = "Call Farmer"
    delay = 0
    count = 0
    last_repayment_date = ""
    last_repayment_amount = ""

    for ticket in tickets[1:]:
       try:
         # Do this for three pilot agents only.
        # TODO: Remove this when you go full live
        if not str(ticket[field_map['loanofficerid']])  in ("29","57","58","6","7"):
            continue
        # sleep the process for a few seconds so that Zoho does not time out due to many
        # requests.
        if count % 200 == 0:
            time.sleep(61)

        if ticket and ticket[field_map['arrearsdays']]:
            delay = int(ticket[field_map['arrearsdays']])
        else:
            delay = 0

        # Check if ticket exists -> UPDATE
        zoho_ticket = get_ticket(zoho_desk_api, cf_loan_id=ticket[field_map['accountno']],
                                 cf_farmer_id=ticket[field_map['clientid']])
        # after zoho call, increment count
        count += 1
        if not zoho_ticket and delay == 0:
            continue

        # Check in our DB to see if this loan officer is already created at Zoho as an agent
        assignee_id = get_zoho_id(ticket[field_map['loanofficerid']])
        if not assignee_id:
            # this loan officer is not mapped at Zoho yet. Let us create him
            zoho_agent= create_agent(ticket)
            if zoho_agent:
              # map this agent to musoni
              assignee_id = zoho_agent['id']
              create_agent_mapping(ticket[field_map['loanofficerid']], assignee_id)

              # after zoho call, increment count
              count += 1
            else:
              # This agent is not created so let us skip this ticket
              continue
        if zoho_ticket:
            # Get last repayment date from Musoni. This is slowing down the task. Change it to get the info from locally saved data.
            repayments_details = get_last_loan_repayment_date_with_amount(ticket[field_map['accountno']])
         
            if not isinstance(repayments_details, str):
                last_repayment_amount = int(repayments_details['amount'])
                last_repayment_date = repayments_details['date']
        
            updated_ticket_data = {
                'subject': "{} - {} days - {}".format(
                    ticket[field_map['clientname']],
                    ticket[field_map['arrearsdays']],
                    ticket[field_map['arrearsamount']]
                ),
                #'priority': priority,
                #'status': status,
                'cf': {
                    'cf_total_arrears': ticket[field_map['arrearsamount']],
                    'cf_delay': ticket[field_map['arrearsdays']],
                    'cf_delay_reason': '',
                    #'cf_follow_action': cf_follow_action, # should be what is there already
                    'cf_last_repayment_date': last_repayment_date,
                    'cf_last_repayment_amount': last_repayment_amount,
                    'cf_total_repayments': ticket[field_map['totalrepaymentderived']]
                },
               #  'assignee_id': assignee.get('id', None)
                'assigneeId':assignee_id
            }
            # logger.debug(updated_ticket_data)
            #logger.debug("assigneeId: " + str(assignee_id))
            up = zoho_desk_api.update_ticket(zoho_ticket['id'], updated_ticket_data)
            # after zoho api call, increment counter
            count += 1
            #logger.debug("Updated ticket -> {}".format(up.get('id', None)))
        else:
            # Check that the contact first
          existing_contact = zoho_desk_api.get_contact({
                "customField1": "cf_farmer_id:${" + (ticket[field_map['clientid']]) + "}"
            })
          if not existing_contact:
                # logger.debug("Farmer not created: " + str(ticket[field_map['clientid']]))
                continue # if the farmer is not created yet, skip this ticket. It will be created automatically once the farmer is created
          else:
                # logger.debug("we found farmer: " + existing_contact['id'])

            repayments_details = get_last_loan_repayment_date_with_amount(ticket[field_map['accountno']])
         
            if not isinstance(repayments_details, str):
                last_repayment_date = repayments_details['date']
                last_repayment_amount = int(repayments_details['amount'])

            new_ticket_data = {
                'subject': "{} - {} days - ksh {} ".format(
                    ticket[field_map['clientname']],
                    ticket[field_map['arrearsdays']],
                    ticket[field_map['arrearsamount']]
                ),
                'description': "Musoni generated ticket, contains the days in arrears, client details and "
                               "total amount in arrears. The ticket will does not contain repayment schedules.",
                'departmentId': settings.ZOHO_DESK_DEP_ID,
                'contactId': existing_contact['id'],
                'priority': 'High',
                'status': 'Open',
                'phone': ticket[field_map['phonenumber']],
                'cf': {
                    'cf_total_arrears': ticket[field_map['arrearsamount']],
                    'cf_farmer_id': ticket[field_map['clientid']],
                    'cf_loan_id': ticket[field_map['accountno']],
                    'cf_ticket_type': 'Loan Follow-Up',
                    'cf_delay': ticket[field_map['arrearsdays']],
                    'cf_delay_reason': '',
                    'cf_last_repayment_date': last_repayment_date,
                    'cf_last_repayment_amount': last_repayment_amount,
                    'cf_loan_start_date': ticket[field_map['disbursedondate']],
                    'cf_follow_action': 'Call Farmer',
                    'cf_total_repayments': ticket[field_map['totalrepaymentderived']]
                },
                'assigneeId': assignee_id
            }
                #logger.debug("assigneeId: " + str(assignee_id))
            ct = zoho_desk_api.create_ticket(new_ticket_data)
            # after each zoho call, increment counter
            count += 1
            #logger.debug("Created ticket -> {}".format(ct.get('id', None)))
       except:
          logger.debug("Creating this ticket failed. " +str(traceback.format_exc()))
    logger.debug("All ticket(s) created")
    return


def create_farmer(data):
    farmer = zoho_desk_api.create_contact(data)
    logger.debug("Successfully created farmer {}".format(farmer.get('id')))
    return farmer


def create_ticket(data):
    ticket = zoho_desk_api.create_contact(data)
    print("Successfully created ticket")
    return ticket


def create_agent_mapping(musoni_id, zoho_id):
    ZohoTb().create_tb_mapping(musoni_id, zoho_id)

def get_zoho_id(musoni_id):
    return ZohoTb().get_agent_mapping(musoni_id)


def prepare_agent_list():
    global AGENTS
    if not AGENTS:
        AGENTS = ZohoAgents(
            zoho_auth_token=settings.ZOHO_DESK_AUTH_TOKEN,
            orgid=settings.ZOHO_DESK_ORG_ID
        )

def create_farmers_background():
    t = threading.Thread(target=bulk_create_farmer(), args=(), kwargs={})
    t.setDaemon(True)
    t.start()

if __name__ == '__main__':
    # bulk_create_farmer()
    bulk_create_ticket()
