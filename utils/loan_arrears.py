import csv

PAR = 2
START = 15

# with open('/home/mutiga/Projects/tulaa-musoni-reports-service/utils/LoanofficerArrearsJustin.csv', 'r') as arrears:
#     arrears_csv = csv.reader(arrears, delimiter=',')
#     arrears_arr = []

#     for a in arrears_csv:
#         arrears_arr.append(a)


def get_loan_arrears_agent(arrears_arr):
    """
    return data in format
    # Locate the PAR %
    # Locate columns of interests [loanOfficer, totaldue, daysinarrears,farmers]

    {
        totaldue,
        daysinarrears,
        farmers

    }
    """

    row = 0
    par_prc = None
    total_overdue = None
    column = PAR - 1 
    column2 = PAR - 1 
    for each in arrears_arr:
        
        if each[column].replace(' ', '').lower() == 'par1%':
            column += 1
            while len(each[column]) == 0:
                column += 1
            par_prc = each[column]
           
        if each[column2].replace(' ', '').lower() == 'totaloverdue':
            column2 += 1
            while len(each[column2]) == 0:
                column2 += 1
            total_overdue = each[column2]
           
        if  total_overdue and par_prc:
            break
        row +=1
    
    field_to_match = ['name', 'daysinarrears']
    field_match_dict = {}
    result = []
    loan_details = {
        'par': par_prc,
        'total_overdue': total_overdue,
        'farmer': [],
    }
    farmer_detail = {}
    
    # Get a mapping of the columns of interest
    for index,field in enumerate(arrears_arr[START]):
        field_lower = field.replace(' ', '').lower()
        if  field_lower in field_to_match:
            # index = field_to_match.index(field_lower)
            field_match_dict[field_lower] = index

    for record in arrears_arr[START+1:]:
        q = any([ len(o) > 0 for o in record])
        if q:
            loan_details['farmer'].append({
                'name': record[field_match_dict['name']],
                'days_arrears': record[field_match_dict['daysinarrears']]
            })
    result.append(loan_details)
    return result


