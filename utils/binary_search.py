"""
A binary search function used to locate a farmers phone number.
"""
from musoni import settings
logger = settings.logger

def binary_search(data, key, field, return_type=1):
    """
    :param return_type:  0=>index where the key is found. 1=>the actual data
    :param data: the dataset to search
    :param key: the unique identifier
    :param field: the column to compare against the key for the search (this column must be sorted)
    :return: farmers phone number
    """
    # key = key.replace('-', '')
    if len(data) == 0:
        if return_type == 1:
            return "Empty data set"
        else:
            return -1
    if len(data) == 1:
        if int(data[0][field].replace('-', '')) == key:
            if return_type == 1:
                return data[0]
            else:
                return 0
        else:
            # print("problem: " + str(data))
            # print(key)
            if return_type == 1:
                return 'Not found'
            else:
                return -1

    mid = len(data) // 2

    if key == int(data[mid][field].replace('-', '')):
        if return_type == 1:
            return data[mid]
        else:
            return mid

    if key < int(data[mid][field].replace('-', '')):
        return binary_search(data[0:mid], key, field, return_type)

    if key > int(data[mid][field].replace('-', '')):
        return binary_search(data[mid:], key, field, return_type)


# This binary seearch is able to return the index
def binary_search_index(data, low, high, key, field, return_type=1):
    """
    :param high:
    :param low:
    :param return_type:  0=>index where the key is found. 1=>the actual data
    :param data: the dataset to search
    :param key: the unique identifier
    :param field: the column to compare against the key for the search (this column must be sorted)
    :return: farmers phone number
    """

    if high < low:
        return -1
    # key = key.replace('-', '')
    if len(data) == 0:
        if return_type == 1:
            return "Empty data set"
        else:
            return -1
    if len(data) == 1:
        if int(data[0][field].replace('-', '')) == key:
            if return_type == 1:
                return data[0]
            else:
                return 0
        else:
            # print("problem: " + str(data))
            # print(key)
            if return_type == 1:
                return 'Not found'
            else:
                return -1

    mid = low + ((high-low) // 2)
    if mid > len(data)-1 or mid < 0:
        logger.debug("problem")
        logger.debug("mid: " + str(mid))
        logger.debug("key: " + str(key))
        return -1
    if key == int(data[mid][field].replace('-', '')):
        if return_type == 1:
            return data[mid]
        else:
            return mid

    if key < int(data[mid][field].replace('-', '')):
        return binary_search_index(data, low, mid-1, key, field, return_type)

    if key > int(data[mid][field].replace('-', '')):
        return binary_search_index(data, mid+1, high, key, field, return_type)
