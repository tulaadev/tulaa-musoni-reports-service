import csv


def portfolio_trend_day(payments, cashflow,header, column):
    row = 0
    for record in payments:
        if record[column].replace(' ', '').lower() == cashflow.replace(' ', '').lower():
            break
        row += 1

    row += 2

    max_empty = 3
    del_columns = []
    column_history = []
    if payments[row][column].replace(' ', '').lower() == header.replace(' ', '').lower():
        payments[row][column] = payments[row][column].replace(':', '').strip()
        # locate all empty columns to delete on the second run/
        while column < len(payments[row]):
            if not payments[row][column]:
                del_columns.append(column)
            column += 1

    # delete empty columns
    payments = payments[row:]

    for each_column in del_columns[::-1]:
        for i in range(len(payments) - 1):
            try:
                del payments[i][each_column]
            except:
                continue


    # Remove remaining none important fields
    for k, v in enumerate(payments):
        if not v[0]:
            return payments[:k]


# with open('/home/mutiga/Projects/tulaa-musoni-reports-service/utils/repayment_branch.csv') as repayments_csv:
#     paymentslst = csv.reader(repayments_csv, delimiter=',')
#     payments_arr = []
#     # Iterate over the csv and create an array
#     for line in paymentslst:
#         # import pdb; pdb.set_trace()
#         payments_arr.append(line)
#
#     # print(payments_arr)
#     print(portfolio_trend_day(payments_arr, 'Cashflow per day', 0))
