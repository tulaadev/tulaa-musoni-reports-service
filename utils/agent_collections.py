import csv
import datetime
import logging
from time import time
import requests
import json

from urllib.parse import quote
from utils.helpers import generate_field_mappings
from utils.binary_search import binary_search
from auth_custom.authentication import read_from_pickle
from musoni import settings

logger = settings.logger


def loan_officer_collections_per_farmer(data, agent_id):
    try:
        tt = time()
        # fields = [
        #     "Total Due",
        #     "Amount repaid",
        #     'Client Name:',
        #     'Expected Repayment',
        #     'Arrears',
        #     'Loan Status:',
        #     'Client ID:',
        #     'Account No:'
        # ]
        fields = [
            'Client Name',
            'Phone Number',
            'Account Number',
            'Total Repaid',
            'Reversed',
            'Submitted On Date',
            'loan_officer_id',
            'arrears'
        ]
        # perform a field mapping to identify relevant columns needed for matching
        start = 0
        field_map = generate_field_mappings(fields, data[start])

        # Perform summation for each client's repayments
        repaid_amount = 0
        farmers = []
        today = datetime.datetime.today().date()
        start_of_month = datetime.datetime.today().date().replace(day=1)
        # We are using the collections report which actually had transactions report to get the collections
        for repayment in data[start + 1:]:
            submitted_date = datetime.datetime.strptime(repayment[field_map['submittedondate']], "%Y-%m-%d").date()

            if repayment and str(repayment[field_map['loan_officer_id']]) == str(agent_id) and today >= submitted_date >= start_of_month and repayment[field_map['reversed']] == 'false':
                # amount_repaid = float(repayment[field_map['amountrepaid']].replace(',', ''))
                amount_repaid = float(repayment[field_map['totalrepaid']].replace(',', '')) if repayment[
                    field_map['totalrepaid']] else 0.00
                repaid_amount += amount_repaid
                if amount_repaid > 0.00:
                    number = repayment[field_map['phonenumber']]
                    arrears = float(repayment[field_map['arrears']].replace(',', '')) if repayment[
                        field_map['arrears']] else 0.00
                    farmers.append({
                        "name": repayment[field_map['clientname']],
                        "repaid": amount_repaid,
                        "arrears": arrears,
                        'number': number,
                        'last_repayment_date': repayment[field_map['submittedondate']]
                    })

        results = {
            "collections": repaid_amount,
            "farmers": farmers

        }
        logging.info("Time taken" + str(time() - tt))
        return results
    except:
        raise


def loan_officer_accrued_par(data):
    fields = [
        "Client Name:",
        "Amount repaid",
        "Expected Repayment",
        "Total Due",
        "Client Name",
        "Arrears",
        "Loan Status :"
    ]
    field_map = generate_field_mappings(fields, data[7])
    total_arrears = 0
    total_expected_repaid_amount = 0

    actual_repaid_amount = 0
    actual_expected_amount = 0
    farmers = []
    for repayment in data[8:-1]:

        if repayment[field_map['loanstatus:']] and repayment[field_map['loanstatus:']] == 'Active':
            expected_repaid_amount = float(
                repayment[field_map['expectedrepayment']].replace(',', ''))
            if expected_repaid_amount > 0:
                arrears = float(
                    repayment[field_map['arrears']].replace(',', ''))
                actual_repaid_amount = float(
                    repayment[field_map['amountrepaid']].replace(',', ''))
                total_expected_repaid_amount += expected_repaid_amount
                total_arrears += arrears
                par = arrears / expected_repaid_amount * 100
                farmers.append({
                    "Client Name": repayment[field_map['clientname:']],
                    "Amount Repaid": actual_repaid_amount,
                    "Expected Repayment": expected_repaid_amount,
                    "Accured Par": round(par, 2),
                    "Arrears": arrears
                })
    if total_expected_repaid_amount > 0:
        accrued = total_arrears / total_expected_repaid_amount * 100
    else:
        accrued = 0
    return {
        "Accrued PAR 30%": round(accrued, 2),
        "farmers": farmers
    }


def get_last_loan_repayment_date(loan_id):
    loan_details = requests.get(
        settings.MUSONI_URL + quote('/loans/{}'.format(loan_id)),
        auth=(settings.MUSONI_USER, settings.MUSONI_PWD),
        params={
            'associations': 'transactions'
        },
        headers={
            'X-Fineract-Platform-TenantId': 'tulaa'
        }
    )
    transactions = json.loads(loan_details.content.decode('utf-8'))['transactions']
    for record in reversed(transactions):
        if record['type']['repayment']:
            return "-".join(map(str, record.get('date')))
    return ''


def get_last_loan_repayment_date_with_amount(loan_id):
    loan_details = requests.get(
        settings.MUSONI_URL + quote('/loans/{}'.format(loan_id)),
        auth=(settings.MUSONI_USER, settings.MUSONI_PWD),
        params={
            'associations': 'transactions'
        },
        headers={
            'X-Fineract-Platform-TenantId': 'tulaa'
        }
    )
    transactions = json.loads(loan_details.content.decode('utf-8'))['transactions']
    for record in reversed(transactions):
        if record['type']['repayment']:
            return {
                'date': "-".join(map(str, record.get('date'))),
                'amount': record.get('amount')
            }
    return ''
# rest = get_last_loan_repayment_date(284)
# print(rest)
