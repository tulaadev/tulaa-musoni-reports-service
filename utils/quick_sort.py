# This function takes last element as pivot, places
# the pivot element at its correct position in sorted
# array, and places all smaller (smaller than pivot)
# to left of pivot and all greater elements to right
# of pivot
import sys

from auth_custom.authentication import read_from_pickle
from musoni import settings
import copy

sys.setrecursionlimit(6000)


def partition(arr, low, high, index):
    i = (low - 1)  # index of smaller element
    pivot = arr[high]  # pivot

    for j in range(low, high):

        # If current element is smaller than the pivot
        v = int(arr[j][index].replace('-', ''))
        pivot_value = int(pivot[index].replace('-', ''))
        if v < pivot_value:
            # increment index of smaller element
            i = i + 1
            arr[i], arr[j] = arr[j], arr[i]

    arr[i + 1], arr[high] = arr[high], arr[i + 1]
    return i + 1


# The main function that implements QuickSort
# arr[] --> Array to be sorted,
# low  --> Starting index,
# high  --> Ending index

# Function to do Quick sort

def quick_sort(arr, low, high, index=5):
    if low < high:
        # pi is partitioning index, arr[p] is now
        # at right place
        pi = partition(arr, low, high, index)

        # Separately sort elements before
        # partition and after partition
        quick_sort(arr, low, pi - 1, index)
        quick_sort(arr, pi + 1, high, index)


if __name__ == '__main__':
    client_data = read_from_pickle(settings.BASE_DIR + '/data/' + settings.CLIENT_DATA)
    print('Client_data before sorting', client_data[1:])

    client_data.pop()
    data = copy.deepcopy(client_data)
    quick_sort(data[1:], 0, len(data[1:]) - 1)

    print('Client_data after sorting', data)
