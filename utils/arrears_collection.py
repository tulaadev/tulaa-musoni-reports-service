import csv


def arrears_collection_amount(arrears, column, field):
    """
    Able to find values in these reports:
    - Institution Arrears Report
    - Expected Repayments Report.
    :param arrears:
    :param column:
    :param field:
    :return:
    """
    row = 0
    while row < 10:
        if arrears[row][column] and arrears[row][column].replace(' ', '').lower() == field.replace(' ', '').lower():
            for i in range(5):
                column += 1
                if arrears[row][column]:
                    return arrears[row][column]
            return 0
        row += 1
    return 'Could not find'


# with open('/home/mutiga/Documents/tulaa/musoni/utils/repayments.csv') as arrears:
#     loans = csv.reader(arrears, delimiter=',')
#     loans_arr = []
#     # Iterate over the csv and create an array
#     count = 0
#     for line in loans:
#         if count < 10:
#             loans_arr.append(line)
#         else:
#             break
#         count += 1
#     print(arrears_collection_amount(loans_arr, 0, 'actual amount repaid'))

