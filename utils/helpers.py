import csv
import json
import requests
from musoni import settings
import datetime
from django.http import JsonResponse
import re
import logging
from auth_custom import authentication
from musoni import settings
logger = settings.logger


def convert_txt_to_matrix(txt, agent_id=-1):
    rst_arr = txt.text.split('\n')
    loans = csv.reader(rst_arr, delimiter=',')
    loans_arr = []
    # Iterate over the csv and create an array
    for line in loans:
        loans_arr.append(line)
    return loans_arr


def convert_json_to_csv(json, destination):
    toCSV = json
    keys = toCSV[0].keys()
    with open('{}.csv'.format(destination), 'w', encoding='utf8', newline='') as f:
        dict_writer = csv.DictWriter(f, keys)
        dict_writer.writeheader()
        dict_writer.writerows(toCSV)


def convert_matrix_to_json(matrix, destination, end, start=0):
    with open('{}.csv'.format(destination), 'w', encoding='utf-8', newline='') as f:
        rcsv = csv.writer(f)
        for row in matrix:
            rcsv.writerow(row)


def load_data(path):
    # Read the contents into memory for processing
    with open(path, 'r') as f:
        data = csv.reader(f, delimiter=';')
        data_arr = []
        for record in data:
            data_arr.append(record)

    return data_arr


def generate_field_mappings(fields, data):
    """
    Get a mapping of the columns of interest
    :param fields: a list of the column headers
    :param data: list of the headers of the csv.
    :return:
    """
    mapping = {}
    fields = [f.replace(' ', '').lower() for f in fields]

    for index, field in enumerate(data):
        field_lower = field.replace(' ', '').lower()
        if field_lower in fields:
            # index = field_to_match.index(field_lower)
            mapping[field_lower] = index
    return mapping


def map_agent(agent_id):
    url = settings.LOAN_OFFICER_MAPPING_URL+'/order/musoni/loan-officer/{}'.format(agent_id)
    agent = requests.get(
        url,
        headers={
            'Authorization': 'Bearer {}'.format(authentication.read_from_pickle(settings.PICKLE_NAME)),
            'programId': settings.PROGRAM_ID
        }
    )
    if agent.status_code == 404:
        return "Not found"

    if agent.status_code != 200:
        logging.critical(str(agent.status_code))
        logging.critical(str(agent.content))
        return "Error {}".format(agent.content)

    return json.loads(agent.content.decode('utf-8'))


# Create your views here.
def validate_date(date_input):
    date_re = re.compile(
        r'(?P<year>\d{4})-(?P<month>\d{2})-(?P<day>\d{2})$'
    )
    date = date_re.match(date_input)
    if date:
        kw = {k: int(v) for k, v in date.groupdict().items()}
        date = str(datetime.date(**kw))
        return date
    else:
        return JsonResponse({'Error': "Wrong date format, format is yyyy-MM-dd"})


















