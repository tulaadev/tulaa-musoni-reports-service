from csv import writer
from csv import reader


# from dashboards.loan_schedule import transform_row


def add_column_in_csv(input_file, output_file, transform_row, duplicate=True):
    try:
        """ Append a column in existing csv using csv.reader / csv.writer classes"""
        # Open the input_file in read mode and output_file in write mode
        with open(input_file, 'r') as read_obj, \
                open(output_file, 'w', newline='') as write_obj:
            # Create a csv.reader object from the input file object
            csv_reader = reader(read_obj, delimiter=';', quotechar='"')
            # Create a csv.writer object from the output file object
            csv_writer = writer(write_obj, delimiter=';', quotechar='"')
            # Read each row of the input csv file as list
            for row in csv_reader:
                # Pass the list / row in the transform function to add column text for this row
                transform_row(row, csv_reader.line_num)
                # Write the updated row / list to the output file
                # before writing, we need to denomalize the data since it's for reports
                # put data to the final column <TB_PAR>
                if csv_reader.line_num > 1:
                    if str(row[11]).replace(",", "") == str(row[1]).replace(",", ""):
                        row.append(str(row[11]).replace(",", ""))
                        csv_writer.writerow(row)
                    else:
                        new_row = row[:]
                        row.append(str(row[1]).replace(",", ""))
                        csv_writer.writerow(row)
                        if duplicate:
                            new_row.append(str(new_row[11]).replace(",", ""))
                            csv_writer.writerow(new_row)
                else:
                    # the first line has header so just write it
                    csv_writer.writerow(row)
    except:
        raise


def add_column_in_csv_reusable(input_file, output_file, transform_row):
    try:
        """ Append a column in existing csv using csv.reader / csv.writer classes"""
        # Open the input_file in read mode and output_file in write mode
        with open(input_file, 'r') as read_obj, \
                open(output_file, 'w', newline='') as write_obj:
            # Create a csv.reader object from the input file object
            csv_reader = reader(read_obj, delimiter=';', quotechar='"')
            # Create a csv.writer object from the output file object
            csv_writer = writer(write_obj, delimiter=';', quotechar='"')
            # Read each row of the input csv file as list
            for row in csv_reader:
                # Pass the list / row in the transform function to add column text for this row
                transform_row(row, csv_reader.line_num)
                # Write the updated row / list to the output file
                csv_writer.writerow(row)
    except:
        raise
