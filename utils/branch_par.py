import csv


OVER_180 = 'Over 180 Days'
RANGES = {
    '1-30': ['1-30 Days'],
    '31-60': ['31-60 Days',],
    '61-90': ['61-90 Days'],
    '91-180': ['91-180 Days'],
    '>180': [OVER_180]
}


def branch_total_par(data, keyword, start, bottom_offset):
    # Find the column index
    column_index = data[start].index(keyword)

    # Locate the amount from the csv
    if column_index != -1:
        # Find the starting row for the amounts row
        row_index = 1

        while any(data[start + row_index]) == False:
            row_index += 1

        # Update the row_index to the starting point of the data results
        row_index += start
        amount = None
        prc = None
        while True:

            if data[row_index][column_index] == 'Amount':
                try:
                    amount = float(data[bottom_offset][column_index].replace(',', ''))
                except ValueError:
                    amount = 0

            if data[row_index][column_index] == '%':
                try:
                    prc = float(data[bottom_offset][column_index].replace('%', ''))
                except ValueError:
                    prc = 0
            if amount is not None and prc is not None:
                return amount, prc
            column_index += 1
    return 0, 0


def branch_total_par_by_ranges(data, filter_range, start, bottom_offset):

    res = []
    res_by_range = {}
    total = 0
    prc = 0
    for r in RANGES[filter_range]:
        result = branch_total_par(data, r, start, bottom_offset)
        total += result[0]
        prc += result[1]

    res_by_range['key'] = filter_range
    res_by_range['amount'] = total
    res_by_range['prc'] = '%.2f' % (prc)

    sum_range = ['1-30 Days', '31-60 Days',
                 '61-90 Days', '91-180 Days', 'Over 180 Days']
    par_sum = 0
    for each in sum_range:
        par_sum += branch_total_par(data, each, start, bottom_offset)[0]
    res_by_range['sum'] = par_sum

    return res_by_range


def branch_total_par_summary(data, start, bottom_offset):
    result = []
    for key in RANGES:
        summary = branch_total_par_by_ranges(data, key, start, bottom_offset)
        result.append(summary)
    return result


# with open('loans.csv') as loan_csv:
#     loans = csv.reader(loan_csv, delimiter=',')
#     loans_arr = []
#     # Iterate over the csv and create an array
#     for line in loans:
#         # import pdb; pdb.set_trace()
#         loans_arr.append(line)
#
#
# toCSV = branch_total_par_summary(loans_arr, 9, -3)
# keys = toCSV[0].keys()
# with open('cleaned_loans.csv', 'w', encoding='utf8', newline='') as f:
#     dict_writer = csv.DictWriter(f, keys)
#     dict_writer.writeheader()
#     dict_writer.writerows(toCSV)
