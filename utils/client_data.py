import copy
import logging
from musoni import settings
import requests
from urllib.parse import quote
from auth_custom.authentication import dump_to_pickle
from utils.quick_sort import quick_sort
from time import time

logger = settings.logger


def download_client_data():
    try:
        tt = time()
        url = settings.MUSONI_URL + quote(settings.MUSONI_CLIENT_DATA)
        logger.debug('Fetching client data from Musoni')
        data = requests.get(
            url,
            timeout=120,
            auth=(settings.MUSONI_USER, settings.MUSONI_PWD),
            headers={
                'X-Fineract-Platform-TenantId': 'tulaa'
            }
        )
        logger.debug('client data: ')
        logger.debug(data)
        arr = []
        for row in data.content.decode('utf-8').split('\r\n'):
            arr.append(row.split(';'))
        logger.debug("dumping the content to a data dump")
        with open(settings.BASE_DIR + '/data/clients.csv', 'w', encoding='utf-8', newline='') as f:
            for row in arr:
                f.write(';'.join(row) + '\r')

        arr.pop()
        data = copy.deepcopy(arr[1:])
        quick_sort(data, 0, len(data) - 1)
        data.insert(0, arr[0])

        dump_to_pickle(data, settings.BASE_DIR + '/data/' + settings.CLIENT_DATA)
        logger.debug('Fetch complete -> ')
    except Exception as error:
        raise
