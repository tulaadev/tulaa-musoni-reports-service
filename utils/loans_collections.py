OVER_180 = 'Over 180 Days'
RANGES = ['1-30 Days',
          '31-60 Days',
          '61-90 Days',
          '91-180 Days',
          OVER_180
          ]


def collections(data, start, bottom):
    mapper = []

    # record += start
    for r in RANGES:
        column = data[start].index(r)
        if column != -1:
            # Find the starting row for the amounts row
            row_index = 1
            while not any(data[start + row_index]):
                row_index += 1

            # Update the row_index to the starting point of the data results
            row_index += start

            # Map out all index locations for each range

            amount = None
            prc = None
            while True:
                if data[row_index][column] == 'Amount':
                    amount = column

                if data[row_index][column] == '%':
                    prc = column
                if amount is not None and prc is not None:
                    mapper.append({
                        r: {
                            'amount': amount,
                            'prc': prc
                        }
                    })
                    break
                column += 1
    # Now with the mapping in place, we can get the filtered out loan collections by loans officer
    parsed = []

    for officer in data[row_index + 1:bottom]:
        x = {}
        for rec in mapper:
            for key in rec:
                par_amount = officer[rec[key]['prc']]
                x[key.replace(' ', '')] = float(par_amount.replace('%',''))
                # x.append({[key]: r[key]['amount']})
        x['loanOfficer'] = officer[0]
        parsed.append(x)

    return parsed
