from musoni import settings
from utils.helpers import load_data
from utils.binary_search import binary_search

logger = settings.logger

# load data from arrears file
arrears_data = load_data(settings.BASE_DIR + '/data/arrears.csv')


def transform_schedule_all(line, line_num):
    if line_num == 1:
        line.append("firstLoanOfficer")
        line.append('loanStatus')
        line.append('Start_Date')
    else:
        # clean loan officer name
        line[1] = str(line[1]).replace(",", "")

        # find loan officer in arrears data
        client_more_data = binary_search(arrears_data[1:], int(line[10]),
                                         9)  # 10 is the account number , 9 is the one in arrears

        if client_more_data == 'Not found':
            line.append(client_more_data)
        else:
            line.append(str(client_more_data[7]).replace(",", ""))
            line.append(client_more_data[11])
            # line.append(client_more_data[5])
            line.append(client_more_data[14])
