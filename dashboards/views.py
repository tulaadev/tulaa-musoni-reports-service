from django.http import HttpResponse
from musoni import settings


def loan_schedule_view(request):
    with open(settings.BASE_DIR + '/data/extended_loan_schedule.csv', 'r', encoding='utf-8', newline='') as f:
        return HttpResponse(f)


def loan_schedule_view_all(request):
    with open(settings.BASE_DIR + '/data/loan_schedule_all.csv', 'r', encoding='utf-8', newline='') as f:
        return HttpResponse(f)
