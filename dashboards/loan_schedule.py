import copy
import sys

import requests
import logging
from urllib.parse import quote
import traceback

from dashboards.loan_schedule_all import transform_schedule_all
from musoni import settings
from utils import CSV_helper
from utils.binary_search import binary_search
from utils.helpers import load_data
from utils.quick_sort import quick_sort

logger = settings.logger

# client_data = load_data(settings.BASE_DIR + '/data/clients.csv')
arrears_data = load_data(settings.BASE_DIR + '/data/arrears.csv')

#
# def sort_arrears():
#     quick_sort(arrears_data, 1, len(arrears_data) - 1, 9)


def transform_row(line, line_num):
    # if this is the first line, append the header
    # if 72 < line_num < 77:
    #     logger.debug("line " + str(line_num))
    #     logger.debug("client data len: " + str(len(client_data)))
    #     logger.debug("client ID: " + str(line[3]))
    #     logger.debug(int(line[3]))

    # logger.debug("line data " + str(line))
    if line_num == 1:
        line.append("First_loan_officer")
        line.append('Loan_status')
        line.append('Arears_Days')
        line.append('Start_Date')
        line.append("TB_PAR")
        # line.append("Disbursed on ")
    else:
        # clean loan officer name
        line[1] = str(line[1]).replace(",", "")
        # look for the first loan officer in the arrears data
        client_more_data = binary_search(arrears_data[1:], int(line[10]), 9)  # 10 is the account number in schedule, 9 is the one in arrears
        if client_more_data == 'Not found':
            line.append(client_more_data)
        else:
            line.append(str(client_more_data[7]).replace(",", ""))
            line.append(client_more_data[11])
            line.append(client_more_data[5])
            line.append(client_more_data[14])
    # if 73 < line_num < 77:
    #     logger.debug("client_more_data " + str(client_more_data))


def fetch_loan_schedule():
    try:
        url = settings.MUSONI_URL + quote(settings.MUSONI_LOAN_SCHEDULE)
        logger.debug('Fetching the loan schedule data from Musoni')

        data = requests.get(
            url,
            auth=(settings.MUSONI_USER, settings.MUSONI_PWD),
            headers={
                'X-Fineract-Platform-TenantId': 'tulaa'
            }
        )
        # put this data in an array of lists
        # sort the array
        # write it to file

        arr = []
        for row in data.content.decode('utf-8').split('\r\n'):
            arr.append(row.split(';'))
        # logger.debug("dumping the content to a data dump")

        # remove the last value as it is invalid
        arr.pop()

        # data_sort = copy.deepcopy(arr[1:])

        quick_sort(arr, 1, len(arr) - 1, 10)
        # data_sort.insert(0, arr[0])

        with open(settings.BASE_DIR + '/data/loan_schedule.csv', 'w', encoding='utf-8', newline='') as f:
            for row in arr:
                f.write(';'.join(row) + '\r')

        # with open(settings.BASE_DIR + '/data/loan_schedule.csv', 'w', encoding='utf-8', newline='') as f:
        #     for row in data.content.decode('utf-8'):
        #         f.write(row)

        logger.debug('Fetch complete')
        # sort arrears data so that we can use it (Comment this. Arrears are already sorted)
        # sort_arrears()
        # now modify the file to add first loan officer column
        CSV_helper.add_column_in_csv(settings.BASE_DIR + '/data/loan_schedule.csv',
                                     settings.BASE_DIR + '/data/extended_loan_schedule.csv', transform_row)

        CSV_helper.add_column_in_csv(settings.BASE_DIR + '/data/loan_schedule.csv',
                                     settings.BASE_DIR + '/data/loan_schedule_all.csv', transform_schedule_all, False)
    except:
        logger.error("An error occurred while processing the loan schedule data: " + str(traceback.format_exc()))
