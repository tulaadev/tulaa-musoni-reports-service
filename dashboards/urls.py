from django.urls import path
from .views import loan_schedule_view
from .views import loan_schedule_view_all
urlpatterns = [
    path('loan_schedule', loan_schedule_view),
    path('loan_schedule_all', loan_schedule_view_all)
]
